export class HParams {

    public static mockAPI = false
    public static powerLossDetection = true

    public static deviceNbI2cModules = 4
    public static deviceNbBanks = 2
    public static deviceGroupSize = 8
    public static deviceI2cFirstAddress = 0x20
    public static deviceI2cFirstRegister = 0x12
    public static deviceI2cFirstInitRegister = 0x00
    public static get maxNbDevices() { return HParams.deviceNbI2cModules * HParams.deviceNbBanks * HParams.deviceGroupSize }

    public static deviceFirstId = 1
    public static nbDevices = HParams.maxNbDevices
    public static defaultMasterDeviceId = (HParams.nbDevices - 1) + HParams.deviceFirstId
    public static useMasterDevice = true
    public static deviceNamePrefix = "R"
    public static logI2cReadWrite = false

    public static hSep = "h"
    public static mSep = "'"
    public static sSep = ""

    public static seqShowDevice = false
    public static seqShowCyclesSeconds = false
    public static seqMaxCycles = 5
    public static seqStartTimeStepMinutes = 2

    public static itemDurationSecondsDefault = 60
    public static itemDurationSecondsMax = 600
    public static itemDurationSecondsMin = 5
    public static itemDurationTickIntervalSeconds = 12
    public static itemDurationStepSeconds = 5
    public static itemDefaultNamePrefix = "EV"
    public static itemPauseName = "(pause)"
    
    public static statusShowSeconds = true
    public static statusPollIntervalMs = 2000
    public static statusPollSymbolIntervalMs = 400

}