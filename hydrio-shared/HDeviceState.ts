export class HDeviceState {
    constructor(
        public id: number,
        public state: boolean
    ) { }
}