import * as moment from 'moment'
import { Moment, Duration, MomentInput } from 'moment'
import {
    Dbg, HSequence, HDevice, HPItem, HStatus, HUtils,
    HPowerLoss
} from '.'

export class HydrioSequencer {

    private dbg: Dbg
    private sequence: HSequence
    private pitems: HPItem[]
    private timer: any
    private pitem: HPItem
    private cycle: number
    private started: Moment

    constructor(
        private onDeviceStateToChange: (device: HDevice, newState: boolean, masterState: boolean) => void
    ) {
        this.dbg = new Dbg(this.constructor.name)
    }

    public get activeSequenceLastStarted() { return this.sequence && this.started && moment(this.started) }

    stop() {
        if (!this.sequence) { return }
        const seqName = this.sequence.name
        this.dbg.log(`unloading sequence '${seqName}'`)
        this.stopItem(false)
        this.sequence = undefined
        this.dbg.log(`sequence '${seqName}' unloaded`)
    }

    start(sequence: HSequence,
        isSystemStart = false,
        pl?: HPowerLoss
    ) {
        if (!sequence) {
            this.dbg.log('nothing to load')
            return
        }

        this.stop()
        this.dbg.log(`loading sequence '${sequence.name}'`)
        this.sequence = sequence
        this.started = undefined
        this.cycle = 1

        if (sequence && isSystemStart
            && pl && pl.powerLostOn && pl.seqStartedOn
        ) {
            this.dbg.log("system start after power loss while active sequence, computing shift")
            const interrupted = sequence.getItemAt(pl.powerLostOn, pl.seqStartedOn)
            if (interrupted) {
                this.dbg.log(`active sequence interrupted during item ${interrupted.item.fullName}`)
                const plDuration = moment.duration(moment().diff(pl.powerLostOn))
                this.dbg.log("power loss lasted", HUtils.durationToString(plDuration))
                this.started = interrupted.sequenceStart.clone().add(plDuration)
                this.cycle = interrupted.cycle
                this.dbg.log("sequence start virtually set to",
                    this.started.toLocaleString(), 
                    "cycle", this.cycle,
                    "instead of", sequence.nextStart().toLocaleString())
            } else {
                this.dbg.log("active sequence was not interrupted, starting normally")
            }
        }

        this.buildPItems(this.started)
        this.scheduleNext()
        this.dbg.log(`sequence '${this.sequence.name}' loaded`)
    }

    getStatus() {
        let curItem: HPItem, nextItem: HPItem
        if (this.sequence && this.sequence.items.length) {
            curItem = this.findPItem()
            const nextTime = moment(curItem && curItem.end || moment()).add(1, 's')
            nextItem = this.findPItem(nextTime)
            if (!nextItem) {
                const finished = this.cycle == this.sequence.repeat
                const nextCycle = finished ? 1 : this.started ? this.cycle + 1 : this.cycle
                const started = finished ? undefined : this.started
                const start = this.sequence.nextStart(nextCycle, started)
                const it = this.sequence.items[0]
                const end = moment(start).add(it.durationSeconds, 's')
                nextItem = new HPItem(it, start, end)
            }
        }
        return new HStatus(
            true,
            false,
            undefined,
            this.sequence && this.sequence.id,
            this.started,
            this.cycle,
            curItem,
            nextItem,
            moment()
        )
    }

    private scheduleNext(duration?: Duration) {
        const ms = Math.max(10, duration && duration.asMilliseconds() || 0)
        this.dbg.log('scheduleNext', ms)
        this.timer = setTimeout(() => this.startNextItem(), ms)
    }

    private startNextItem() {
        const newPItem = this.findPItem()
        const isTransition = this.isTransition(newPItem) || this.isCyclesTransition()
        this.stopItem(isTransition)
        if (newPItem) {
            if (!this.started) { this.started = moment() }
            this.startItem(newPItem, isTransition)
            return
        }
        this.dbg.log('no next item' /*, moment().toLocaleString(), '[' + this.pitems.join(', ') + ']'*/)
        let start: Moment
        if (this.started && this.cycle < this.sequence.repeat) {
            this.dbg.log(`end of cycle ${this.cycle}/${this.sequence.repeat}`)
            this.cycle++
            start = this.sequence.nextStart(this.cycle, this.started)
            //this.dbg.log('nextStart', this.startedOn, start)
            this.buildPItems(start)
        } else {
            if (this.started) { this.dbg.log('end of sequence') }
            this.started = undefined
            this.cycle = 1
            start = this.sequence.nextStart()
        }
        const beforeStart = moment.duration(start.diff(moment()))
        this.scheduleNext(beforeStart)
        this.dbg.log(`sequence '${this.sequence.name}'`
            + ` next ${this.cycle == 1 ? 'start' : 'cycle'}`
            + ` scheduled for ${HUtils.isTomorrow(start) ? 'tomorrow ' : ''}`
            + start.format('HH:mm:ss')
        )
    }

    private stopItem(isTransition: boolean) {
        clearTimeout(this.timer)
        if (!this.pitem) { return }
        const item = this.pitem.item
        this.pitem = undefined
        this.dbg.log('stopping item', item && item.fullName)
        if (this.onDeviceStateToChange) {
            const masterState = isTransition ? undefined : false
            this.onDeviceStateToChange(item.device, false, masterState)
        }
    }

    private startItem(pi: HPItem, isTransition: boolean) {
        if (pi.item.isSequenceable) {
            this.dbg.log('starting item ' + pi.item.fullName)
            this.pitem = pi
            const masterState = isTransition ? undefined : true
            this.onDeviceStateToChange(this.pitem.item.device, true, masterState)
        } else {
            this.dbg.log('skipping item ' + pi.item.fullName)
        }
        const durationLeft = moment.duration(pi.end.diff(moment()))
        this.scheduleNext(durationLeft)
        this.dbg.log('next event in about ' + HUtils.durationToString(durationLeft))
    }

    private buildPItems(start?: Moment) {
        start = start || this.sequence.nextStart()
        const pitems = new Array<HPItem>()
        let end: Moment
        this.sequence.items
            .sort((a, b) => (a.position || 0) - (b.position || 0))
            .forEach(it => {
                start = end || start
                end = start.clone().add(it.durationSeconds, 's')
                pitems.push(new HPItem(it, start, end))
            })
        //this.dbg.log('buildPItems', pitems.map(p => p.toString(true)))
        this.pitems = pitems
    }

    private findPItem(m: MomentInput = moment()) {
        return this.pitems && this.pitems.find(pi => pi && pi.matches(m))
    }
    private isTransition(npi: HPItem) {
        const cpi = this.pitem
        return cpi && npi && npi != cpi
            && npi.item.isSequenceable == cpi.item.isSequenceable
    }
    private isCyclesTransition() {
        const cpi = this.pitem, pis = this.pitems
        return cpi && pis.length
            && pis.indexOf(cpi) == pis.length - 1   //last of cycle
            && this.cycle < this.sequence.repeat    //not last cycle
            && cpi.item.isSequenceable == this.sequence.items[0].isSequenceable //same type
    }
}