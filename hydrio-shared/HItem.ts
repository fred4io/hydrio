import { HParams, HDevice } from '.'

export class HItem {

    private _device: HDevice
    private _deviceId: number

    constructor(
        public sequenceId?: number,
        public id?: number,
        public name?: string,
        device?: HDevice | number,
        public durationSeconds = HParams.itemDurationSecondsDefault,
        public position?: number,
        public info?: string
    ) {
        if (device instanceof HDevice) {
            this.device = device
        } else {
            this.deviceId = device
        }
    }

    static fromData(o: HItem, devices?: HDevice[]) {
        const did = o.deviceId || o._deviceId
        const device = devices && devices.find(d => d.id == did) || did
        if (o.id < 1) o.id = undefined
        return new HItem(o.sequenceId, o.id, o.name, device, o.durationSeconds, o.position, o.info)
    }


    get fullName() {
        return (this.name || '')
            + (!this.device ? '' : ('/' + this.device.address))
    }


    get device() { return this._device }
    set device(value: HDevice) {
        this._device = value
        this._deviceId = this._device ? this._device.id : undefined
    }

    get deviceId() { return this._deviceId }
    set deviceId(value: number) {
        this._deviceId = value
        if (this._device && this.device.id != this._deviceId) {
            this.device = undefined
        }
    }

    get isSequenceable() {
        return !!this.device && this.device.isValidForSequence
    }

    copy(it: HItem) {
        this.id = it.id
        this.name = it.name
        this._device = it._device
        this._deviceId = it._deviceId
        this.durationSeconds = it.durationSeconds
        this.position = it.position
        this.info = it.info
        return this
    }

    clone() {
        return new HItem().copy(this)
    }
}
