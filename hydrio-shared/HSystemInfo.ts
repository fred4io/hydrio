export class HSystemInfo {
    constructor(
        public build: string,
        public board: string,
        public i2cEnabled: boolean,
        public pldEnabled: boolean
    ) { }
}