import * as moment from 'moment'
import { Moment } from 'moment'
import { HItem, HUtils, HDevice } from '.'

export class HSequence {

    constructor(
        public id: number,
        public name: string,
        public startTime = "08:00",
        public repeat = 1,
        public items = new Array<HItem>(),
    ) {
    }

    public static fromData(s: HSequence, devices?: HDevice[]) {
        const items = s.items && s.items.map(o => HItem.fromData(o, devices))
        return new HSequence(s.id, s.name, s.startTime, s.repeat, items)
    }

    public clone() {
        return new HSequence(this.id, this.name, this.startTime, this.repeat, this.items)
    }
    public copy(o: HSequence, items = this.items) {
        this.id = o.id
        this.name = o.name
        this.startTime = o.startTime
        this.repeat = o.repeat
        this.items = items
    }

    public get cycleSeconds() {
        return this.items.reduce((p, it) => p + (it.durationSeconds || 0), 0)
    }
    public get totalSeconds() {
        return this.cycleSeconds * this.repeat
    }
    public get isLongerThan24h() {
        return this.totalSeconds > 24 * 3600
    }
    public get endTime() {
        const m = this.startMoment
        m.add(this.totalSeconds, 's')
        return m.format('HH:mm:ss')
    }

    public nextStart(cycle = 1, startedOn?: Moment) {
        if (cycle < 1) { throw new Error('invalid cycle') }
        if (cycle > this.repeat) { throw new Error(`sequence has only ${this.repeat} ${this.repeat == 1 ? 'cycle' : 'cycles'}`) }
        const result = moment(startedOn || this.startMoment)
        if (cycle > 1) { result.add((cycle - 1) * this.cycleSeconds, 's') }
        return result
    }
    public previousStart() {
        const result = this.startMoment
        if (result.isAfter(moment())) { result.subtract(1, 'd') }
        return result
    }

    public getItemAt(m: Moment, startedOn?: Moment) {
        const seqStart = moment(startedOn || this.startMoment)
        let itemEnd: Moment
        const items = this.items.sort((a, b) => (a.position || 0) - (b.position || 0))
        for (let cycle = 1; cycle <= this.repeat; cycle++) {
            for (var i = 0; i < items.length; i++) {
                const item = items[i]
                const itemStart = itemEnd || seqStart
                itemEnd = moment(itemStart).add(item.durationSeconds, 's')
                if (itemStart.isSameOrBefore(m, 's') && itemEnd.isSameOrAfter(m, 's')) {
                    return {
                        item: item,
                        itemStart: itemStart,
                        itemEnd: itemEnd,
                        cycle: cycle,
                        sequenceStart: seqStart
                    }
                }
            }
        }
        return undefined
    }

    private get startMoment() {
        const hm = HUtils.splitHMM(this.startTime)
        const m = moment().hour(hm.h).minute(hm.m).second(0).millisecond(0)
        if (m.isBefore(moment())) { m.add(1, 'd') }
        return m
    }


}