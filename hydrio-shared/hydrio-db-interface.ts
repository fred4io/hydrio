
import * as moment from 'moment'
import { HDevice, HSequence, HSequenceGenerateInfo, HItem, HPowerLoss } from '.'

export interface IHydrioDb {
    initData(devices: HDevice[]): void
    close(): void

    getIsLocked(): boolean
    setIsLocked(locked: boolean): void
    getSingleActive(): boolean
    setSingleActive(singleActive: boolean): void
    getMasterDeviceId(): number
    setMasterDeviceId(deviceId: number): void
    getActiveSequenceId(): number
    setActiveSequenceId(sequenceId: number): void

    getDevices(): HDevice[]
    setDeviceDead(deviceId: number, dead: boolean): void
    
    getSequences(): HSequence[]
    createSequence(info: HSequenceGenerateInfo): HSequence
    updateSequence(seq: HSequence): void
    deleteSequence(sequenceId: number): void

    getSequenceItems(sequenceId: number): HItem[]
    createSequenceItem(item: HItem): number
    updateSequenceItem(item: HItem): void
    updateActiveSequenceItem(item: HItem): void
    deleteSequenceItem(itemId: number): void

    getSequenceIdFromItemId(itemId: number): number
    getSequence(sequenceId: number, withItems: boolean): HSequence

    getStateChanges(from?: moment.MomentInput, to?: moment.MomentInput)
    setStateChange(deviceId: number, newState: boolean, isMaster: boolean): void

    setPowerLost(sequenceStartedOn: moment.Moment)
    getPowerLost(): HPowerLoss
    setPowerRecovered()
}
