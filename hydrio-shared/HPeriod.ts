import * as moment from 'moment'
import { Moment, MomentInput } from 'moment'

export class HPeriod {

    public start: Moment
    public end: Moment

    constructor(start: MomentInput, end: MomentInput) {
        this.start = start == undefined ? undefined : moment(start)
        this.end = end == undefined ? undefined : moment(end)
    }

    get duration() {
        return this.end && this.start
            && moment.duration(this.end.diff(this.start))
    }
    get startsIn() {
        return this.start
            && moment.duration(this.start.diff(moment()))
    }
    get endsIn() {
        return this.end
            && moment.duration(this.end.diff(moment()))
    }

    matches(m: MomentInput) {
        return this.start && this.end
            && this.start.isValid() && this.end.isValid()
            && this.start.isSameOrBefore(m) && this.end.isSameOrAfter(m)
    }
}