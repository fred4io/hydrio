export class I2cData {
    constructor(
        public address: number,
        public register: number,
        public byte: number
    ) { }
}