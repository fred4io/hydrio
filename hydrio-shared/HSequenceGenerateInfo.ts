import { HParams } from '.'

export class HSequenceGenerateInfo {
    constructor(
        public nbItems = 1,
        public itemDurationSeconds = HParams.itemDurationSecondsDefault,
        public insertPause = false,
        public pauseDurationSeconds = 60,
        public name = "Nouvelle séquence",
    ) { }

    public get totalSeconds() {
        return this.nbItems * (
            this.itemDurationSeconds
            + (this.insertPause ? this.pauseDurationSeconds : 0)
        )
    }
    public get totalNoPause() {
        return this.nbItems * this.itemDurationSeconds
    }
}