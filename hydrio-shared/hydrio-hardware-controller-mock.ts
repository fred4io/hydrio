import { Dbg, HParams, IHydrioHardwareController, I2cData } from '.'

export class HydrioHardwareControllerMock implements IHydrioHardwareController {

    private nolog = true
    private dbg: Dbg
    private disposed = false
    private onPowerLoss?: () => void
    private byteMap = new Map<string, number>()

    constructor() {
        this.dbg = new Dbg(this.constructor.name)
    }

    get i2cEnabled() { return true }
    get pldEnabled() { return !!this.onPowerLoss }
    get boardRevision() { return "unknown (mock)" }

    start(
        onPowerLoss?: () => void,
        i2cLog = false
    ) {
        this.onPowerLoss = onPowerLoss
        this.nolog = !i2cLog
        return new Promise<void>(resolve => resolve())
    }

    dispose() {
        this.disposed = true
        this.dbg.log('disposed')
    }

    i2cReadByte(address: number, register: number): number {
        this.checkDisposed()
        if (!this.nolog) { this.dbg.log('i2cReadByte', address, register) }
        return this.byteMap.get(address + '_' + register) || 0
    }
    i2cWriteByte(address: number, register: number, byte: number, noLog = false): void {
        this.checkDisposed()
        if (!noLog && !this.nolog) { this.dbg.log('i2cWriteByte', address, register, byte) }
        this.byteMap.set(address + '_' + register, byte)
    }
    i2cAllOff() {
        this.checkDisposed()
        if (!this.nolog) { this.dbg.log('i2cAllOff') }
        for (let j = 0; j < HParams.deviceNbI2cModules; j++) {
            for (let i = 0; i < HParams.deviceNbBanks; i++) {
                this.i2cWriteByte(HParams.deviceI2cFirstAddress + j, HParams.deviceI2cFirstRegister + i, 0, true)
            }
        }
    }
    i2cReadAll() {
        const result = new Array<I2cData>()
        for (let j = 0; j < HParams.deviceNbI2cModules; j++) {
            for (let i = 0; i < HParams.deviceNbBanks; i++) {
                const adr = HParams.deviceI2cFirstAddress + j,
                    reg = HParams.deviceI2cFirstRegister + i,
                    byte = this.byteMap.get(adr + '_' + reg) || 0
                result.push(new I2cData(adr, reg, byte))
            }
        }
        return result
    }

    simulatePowerLoss() {
        this.dbg.log("simulating powerloss")
        this.onPowerLoss()
    }

    private checkDisposed() {
        if (this.disposed) { throw new Error("disposed") }
    }


}