
export interface II2cController {
    i2cEnabled: boolean
    i2cReadByte(address: number, register: number): number
    i2cWriteByte(address: number, register: number, byte: number): void
    i2cAllOff()
    i2cReadAll(): Array<{ address: number, register: number, byte: number }>
}

export interface IHydrioHardwareController extends II2cController {
    start(
        onPowerLoss?: () => void,
        i2cLog?: boolean
    ): Promise<void>
    pldEnabled: boolean
    boardRevision: string
    dispose()
    simulatePowerLoss()
}