import * as moment from 'moment'
import {
    IHydrioDb, Dbg, HDevice, HSequence, HItem, HSequenceGenerateInfo, HUtils, HParams
} from '.'

export class HydrioDbMock implements IHydrioDb {

    private dbg: Dbg
    private devices: HDevice[]
    private isLocked = false
    private isSingleActive = true
    private masterDeviceId = HParams.defaultMasterDeviceId
    private activeSequenceId: number
    private sequences: HSequence[]
    private maxItemId = 0
    private maxSequenceId = 0

    constructor(generateData = false) {
        this.dbg = new Dbg(this.constructor.name)
        this.sequences = new Array<HSequence>()
        if (generateData) {

            let s = new HSequence(1, "Sequence normale", "08:00", 4)
            s.items = HUtils.generateSequenceItems(s.id,
                new HSequenceGenerateInfo(30), this.devices)
            this.sequences.push(s)

            const startTime = moment().add(2, 'm').format('HH:mm')
            s = new HSequence(2, "Sequence test", startTime, 2)
            s.items = HUtils.generateSequenceItems(s.id,
                new HSequenceGenerateInfo(3, 10, true, 5),
                this.devices)
            this.sequences.push(s)

            this.activeSequenceId = 2
        }
    }

    initData(devices: HDevice[]): void {
        this.dbg.log('initData')
        this.devices = devices.map(HDevice.fromData)
    }
    close(): void {
        this.dbg.log('close')
    }

    getIsLocked(): boolean {
        this.dbg.log('getIsLocked')
        return this.isLocked
    }
    setIsLocked(locked: boolean): void {
        this.dbg.log('setIsLocked', locked)
        this.isLocked = locked
    }
    getSingleActive(): boolean {
        this.dbg.log('getSingleActive')
        return this.isSingleActive
    }
    setSingleActive(singleActive: boolean): void {
        this.dbg.log('setSingleActive', singleActive)
        this.isSingleActive = singleActive
    }
    getMasterDeviceId(): number {
        this.dbg.log('getMasterDeviceId')
        return this.masterDeviceId
    }
    setMasterDeviceId(deviceId: number): void {
        this.dbg.log('setMasterDeviceId', deviceId)
        this.masterDeviceId = deviceId
    }
    getActiveSequenceId(): number {
        this.dbg.log('getActiveSequenceId')
        return this.activeSequenceId
    }
    setActiveSequenceId(sequenceId: number): void {
        this.dbg.log('setActiveSequenceId', sequenceId)
        this.activeSequenceId = sequenceId
    }

    getDevices(): HDevice[] {
        this.dbg.log('getDevices')
        return this.devices.map(HDevice.fromData)
    }
    setDeviceDead(deviceId: number, dead: boolean): void {
        this.dbg.log('setDeviceDead', deviceId, dead)
        const d = this.devices.find(o => o.id == deviceId)
        if (d) { d.isDead = dead }
    }

    getSequences(): HSequence[] {
        this.dbg.log('getSequences')
        return this.sequences.map(s => HSequence.fromData(s, this.devices))
    }
    createSequence(info: HSequenceGenerateInfo): HSequence {
        this.dbg.log('createSequence', info)
        const seq = new HSequence(undefined, info.name)
        seq.id = ++this.maxSequenceId
        seq.items = HUtils.generateSequenceItems(seq.id, info)
        seq.items.forEach(o => o.id = ++this.maxItemId)
        return seq
    }
    updateSequence(seq: HSequence): void {
        this.dbg.log('updateSequence', seq.name)
        const s = seq && this.sequences.find(o => o.id == seq.id)
        if (s) { s.copy(seq) }
    }
    deleteSequence(sequenceId: number): void {
        this.dbg.log('deleteSequence', sequenceId)
        const s = this.sequences.find(o => o.id == sequenceId)
        const i = s && this.sequences.indexOf(s)
        if (i != -1) { this.sequences.splice(i, 1) }
    }

    getSequenceItems(sequenceId: number): HItem[] {
        this.dbg.log('getSequenceItems', sequenceId)
        const s = this.sequences.find(o => o.id == sequenceId)
        return s && s.items && s.items.map(o => HItem.fromData(o, this.devices)) || []
    }
    createSequenceItem(item: HItem): number {
        this.dbg.log('createSequenceItem', item.name)
        const s = this.sequences.find(o => o.id == item.sequenceId)
        const it = HItem.fromData(item, this.devices)
        it.id = ++this.maxItemId
        s.items.push(it)
        return it.id
    }
    updateActiveSequenceItem(item: HItem): void {
        this.dbg.log('updateActiveSequenceItem', item.name)
        const s = this.sequences.find(o => o.id == item.sequenceId)
        const it = s && s.items.find(o => o.id == item.id)
        if (it) {
            it.name = item.name
            it.info = item.info
        }
    }
    updateSequenceItem(item: HItem): void {
        this.dbg.log('updateSequenceItem', item.name)
        const s = this.sequences.find(o => o.id == item.sequenceId)
        const it = s && s.items.find(o => o.id == item.id)
        if (it) { it.copy(item) }
    }
    deleteSequenceItem(itemId: number): void {
        this.dbg.log('deleteSequenceItem', itemId)
        const s = this.sequences.find(o => o.items.some(p => p.id == itemId))
        const it = s.items.find(o => o.id == itemId)
        const i = s.items.indexOf(it)
        if (i != -1) { s.items.splice(i, 1) }
    }

    getSequenceIdFromItemId(itemId: number): number {
        this.dbg.log('getSequenceIdFromItemId', itemId)
        const s = this.sequences.find(o => o.items.some(it => it.id == itemId))
        return s && s.id
    }
    getSequence(sequenceId: number, withItems: boolean): HSequence {
        this.dbg.log('getSequence', sequenceId, withItems)
        let s = this.sequences.find(o => o.id == sequenceId)
        s = s && HSequence.fromData(s)
        if (!withItems) { s.items.length = 0 }
        return s
    }

    getStateChanges(from?: moment.MomentInput, to?: moment.MomentInput) {
        console.log('getStateChanges', from, to)
        return []
    }
    setStateChange(deviceId: number, newState: boolean, isMaster: boolean): void {
        console.log('setStateChange', deviceId, newState, isMaster)
    }
    setPowerLost(sequenceStartedOn: moment.Moment) {
        console.log('setPowerLost, sequenceStartedOn:', sequenceStartedOn)
    }
    getPowerLost() {
        console.log('getPowerLost')
        return undefined
    }
    setPowerRecovered() {
        console.log('setPowerRecovered')
    }
}