import * as moment from 'moment'

export class HPowerLoss  {
    constructor(
        public powerLostOn: moment.Moment,
        public seqStartedOn: moment.Moment
    ) { }
}