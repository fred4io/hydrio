import * as moment from 'moment'
import { HParams, HDevice, HSequenceGenerateInfo, HItem, HSequence, I2cBit } from '.'

// tslint:disable:no-bitwise
export class HUtils {

    public static isTomorrow(m: moment.MomentInput) {
        const tomorrow = moment().startOf('d').add(1, 'd')
        return moment(m).startOf('d').isSame(tomorrow, 'd')
    }
    public static isToday(m: moment.MomentInput) {
        const today = moment().startOf('d')
        return moment(m).startOf('d').isSame(today, 'd')
    }
    public static isYesterday(m: moment.MomentInput) {
        const yesterday = moment().startOf('d').add(-1, 'd')
        return moment(m).startOf('d').isSame(yesterday, 'd')
    }
    public static isBeforeToday(m: moment.MomentInput) {
        const today = moment().startOf('d')
        return moment(m).isBefore(today)
    }
    public static toToday(mi: moment.MomentInput) {
        const m = moment(mi)
        const today = moment().startOf('d')
        today.hour(m.hour()).minute(m.minute()).second(m.second()).millisecond(0)
        return today
    }
    public static durationToString(d: moment.Duration) {
        return !d ?
            'undefined'
            : d.asMinutes() < 1 ?
                (Math.round(d.asSeconds()) + 's')
                : d.humanize()

    }

    public static secondsToMMSS(seconds: number,
        mSep = HParams.mSep, sSep = HParams.sSep
    ) {
        const m = ~~((seconds || 0) / 60), s = (seconds || 0) % 60
        return m + mSep + (s < 10 ? '0' : '') + s + sSep
    }
    public static secondsToHMMSS(seconds: number,
        hSep = HParams.hSep, mSep = HParams.mSep, sSep = HParams.sSep) {
        const h = ~~(seconds / 3600), m = ~~((seconds % 3600) / 60), s = seconds % 60
        return h + hSep
            + (m < 10 ? '0' : '') + m + mSep
            + (s < 10 ? '0' : '') + s + sSep
    }
    public static secondsToHMM(seconds: number,
        hSep = HParams.hSep, mSep = HParams.mSep) {
        const h = ~~(seconds / 3600), m = ~~((seconds % 3600) / 60)
        return h + hSep
            + (m < 10 ? '0' : '') + m + mSep
    }
    public static minutesToHHMM(minutes: number,
        hSep = ':', mSep = '') {
        const h = ~~(minutes / 60), m = minutes % 60
        return (h < 10 ? '0' : '') + h + hSep
            + (m < 10 ? '0' : '') + m + mSep
    }

    public static splitHMM(hmm: string) {
        if (!hmm) { hmm = '0:00' }
        const a = hmm.split(':')
        return { h: parseInt(a[0], 10), m: parseInt(a[1], 10) }
    }
    public static splitHMS(hms: string) {
        if (!hms) { hms = '0:0:0' }
        const a = hms.split(':')
        return { h: parseInt(a[0], 10), m: parseInt(a[1], 10), s: parseInt(a[2] || '0', 10) }
    }
    public static hhmmToMinutes(hmm: string) {
        const hm = HUtils.splitHMM(hmm)
        return hm.h * 60 + hm.m
    }

    public static addSecondsToHMS(hms: string, seconds: number) {
        const a = HUtils.splitHMS(hms)
        const ts = a.s + seconds, s = ts % 60
        const tm = a.m + ~~(ts / 60), m = tm % 60
        const th = a.h + ~~(tm / 60), h = th % 24
        return (h < 10 ? '0' : '') + h
            + ':' + (m < 10 ? '0' : '') + m
            + ':' + (s < 10 ? '0' : '') + s
    }

    public static hexString(n: number) { return n.toString(16) }
    public static fromHexString(h: string) { return parseInt(h, 16) }
    public static bitIndexString(bitIndex: number, length = 8) {
        return this.bitsString((1 << bitIndex))
    }
    public static bitsString(n: number, length = 8) {
        let s = n.toString(2)
        return '0'.repeat(length - s.length) + s
    }
    public static fromBitString(b: string) { return parseInt(b, 2) }

    public static getDeviceParams(deviceIndex: number, groupSize: number, nbBanks: number) {
        return {
            chip: ~~(deviceIndex / (groupSize * nbBanks)),
            bank: ~~((deviceIndex % (groupSize * nbBanks)) / groupSize),
            port: deviceIndex % groupSize
        }
    }

    public static generateDevices() {
        const nb: number = HParams.nbDevices
        const groupSize = HParams.deviceGroupSize
        const nbBanks = HParams.deviceNbBanks
        const startAddress = HParams.deviceI2cFirstAddress
        const startCharCode = 'A'.charCodeAt(0)
        const devices = new Array<HDevice>()
        const id0 = HParams.deviceFirstId
        for (let i = 0; i < nb; i++) {
            const p = HUtils.getDeviceParams(i, groupSize, nbBanks)
            const address = HUtils.hexString(startAddress + p.chip)
                + String.fromCharCode(startCharCode + p.bank)
                + HUtils.hexString(p.port)
            const name = `${HParams.deviceNamePrefix}${i + 1}(${p.chip + 1}.${p.port + 1} ${address})`
            const d = new HDevice(id0 + i, name, address)
            devices.push(d)
        }
        return devices;
    }

    public static generateSequenceItems(
        sequenceId: number,
        info: HSequenceGenerateInfo,
        devices?: HDevice[]
    ) {
        const items = new Array<HItem>()
        const namePrefix = HParams.itemDefaultNamePrefix
        const max = info.insertPause ? (info.nbItems * 2) : info.nbItems
        for (let i = 1; i <= max; i++) {
            const isPause = info.insertPause && i % 2 == 0
            const name = isPause ? "(pause)" : namePrefix + i
            const deviceId = isPause ? null : devices ? devices[i - 1] : i
            const durationSeconds = isPause ? info.pauseDurationSeconds : info.itemDurationSeconds
            items.push(new HItem(sequenceId, i, name, deviceId, durationSeconds, i))
        }
        return items
    }

    public static getNewSequenceItemName(sequence: HSequence) {
        const nbItemsNoPause = sequence.items.reduce((p, it) => it && it.device ? (p + 1) : p, 0)
        return HParams.itemDefaultNamePrefix + (nbItemsNoPause + 1)
    }

}