import { Dbg, HDevice, HUtils, HParams, II2cController, I2cBit, HDeviceState } from '.'

type TOnDeviceStateChanged = (deviceId: number, newState: boolean, isMaster: boolean) => void
export class HydrioDeviceController {

    private canSetAllOffAtOnce = false

    private dbg: Dbg
    private devices: HDevice[]
    private singleActive: boolean
    private masterId: number
    private onDeviceStateChanged: TOnDeviceStateChanged
    private disposed = false

    constructor(
        private i2c: II2cController
    ) {
        this.dbg = new Dbg(this.constructor.name)
        this.devices = HUtils.generateDevices()
    }

    public init(
        onDeviceStateChanged: TOnDeviceStateChanged,
        singleActive: boolean,
        masterId: number
    ) {
        this.dbg.try('init')
        this.onDeviceStateChanged = onDeviceStateChanged
        if (this.i2c.i2cEnabled) {
            this.setAllOff()
            this.setSingleActive(singleActive)
            this.setMaster(masterId)
        } else {
            this.dbg.log('i2c disabled. skipping set i2c state')
            this.singleActive = singleActive
            this.masterId = masterId
            this.devices.forEach(d => d.isReserved = d.id == masterId)
        }
        this.dbg.done('init')
    }

    public dispose() {
        if (this.disposed) { return }
        this.disposed = true
        this.dbg.log('disposing')
        try { this.i2c.i2cAllOff() }
        catch{ }
        this.dbg.log('disposed')
    }

    public isMaster(deviceId) {
        this.checkDisposed()
        return this.masterId == deviceId
    }

    public setSingleActive(singleActive: boolean) {
        this.checkDisposed()
        this.dbg.log('singleActive', singleActive)
        if (this.i2c.i2cEnabled && singleActive) {
            const firstActiveNotMaster = this.devices.find(d =>
                d.id != this.masterId && this.peek(d.id, true))
            const butThis = firstActiveNotMaster ? firstActiveNotMaster.id : undefined
            this.setAllOff(butThis)
        }
        this.singleActive = singleActive
    }

    public setMaster(masterId: number) {
        this.checkDisposed()
        this.dbg.log('setMaster', masterId)
        this.setAllOff(undefined, true)
        this.masterId = masterId
        this.devices.forEach(d => d.isReserved = d.id == masterId)
    }

    public getDevices() {
        this.checkDisposed()
        return this.devices
    }

    public setDead(id: number, dead: boolean) {
        this.checkDisposed()
        this.dbg.log('setDead/' + id, dead)
        const dd = this.devices.find(d => d.id == id)
        if (dd) {
            dd.isDead = !!dead
            if (dd.isDead) {
                try { this.poke(dd.id, false) }
                catch { }
            }
        }
    }

    public getState(id: number) {
        this.checkDisposed()
        this.dbg.log('getState/' + id)
        return this.peek(id)
    }
    public setState(id: number, state: boolean) {
        this.checkDisposed()
        this.dbg.log('setState/' + id, state)
        if (state && this.singleActive && id != this.masterId) {
            this.setAllOff(id)
        }
        this.poke(id, state)
    }

    public getAllStates() {
        this.checkDisposed()
        this.dbg.log('getAllStates')
        const i2cData = this.i2c.i2cReadAll()
        return this.devices.map(d => {
            const bit = this.fromDeviceId(d.id)
            const data = i2cData.find(da => 
                da.address == bit.address 
                && da.register == bit.register)
            return new HDeviceState(d.id, data && bit.isOn(data.byte))
        })
    }

    public setAllOff(butThis?: number, includeMaster = false) {
        this.checkDisposed()
        this.dbg.log('setAllOff', butThis, includeMaster)
        if (this.canSetAllOffAtOnce && butThis == undefined && includeMaster) {
            this.i2c.i2cAllOff()
            //TODO: foreach?
            //if (this.onDeviceStateChanged) {
            //    this.onDeviceStateChanged(address, state, address == this.masterAddress)
            //}
        } else {
            this.devices.forEach(d => {
                if (d.id == butThis || d.id == this.masterId && !includeMaster) {
                    return
                }
                this.poke(d.id, false, true)
            })
        }
    }

    private poke(deviceId: number, state: boolean, noLog = false) {
        state = !!state
        const bit = this.fromDeviceId(deviceId)
        if (!noLog) { this.dbg.log('poke', state, deviceId, bit.toString()) }
        const byte = this.i2c.i2cReadByte(bit.address, bit.register)
        const currentState = bit.isOn(byte)
        const changed = currentState != state

        if (changed) {
            const newByte = bit.update(byte, state)
            this.i2c.i2cWriteByte(bit.address, bit.register, newByte)
            this.dbg.log('changed', deviceId, state ? "off->on" : "on->off")
            if (this.onDeviceStateChanged) {
                this.onDeviceStateChanged(deviceId, state, deviceId == this.masterId)
            }
        }
    }

    private peek(deviceId: number, noLog = false): boolean {
        const bit = this.fromDeviceId(deviceId)
        if (!noLog) { this.dbg.log('peek', deviceId, bit.toString()) }
        const byte = this.i2c.i2cReadByte(bit.address, bit.register)
        return bit.isOn(byte)
    }

    private fromDeviceId(deviceId: number) {
        const dp = HUtils.getDeviceParams(
            deviceId - HParams.deviceFirstId,
            HParams.deviceGroupSize,
            HParams.deviceNbBanks
        )
        return new I2cBit(
            HParams.deviceI2cFirstAddress + dp.chip,
            HParams.deviceI2cFirstRegister + dp.bank,
            dp.port
        )
    }

    private checkDisposed() {
        if (this.disposed) { throw new Error("disposed") }
    }

}