import * as moment from 'moment'
import { Dbg, IHydrioPlatformController } from '.'

export class HydrioPlatformControllerMock implements IHydrioPlatformController {

    private dbg: Dbg

    constructor() {
        this.dbg = new Dbg(this.constructor.name)
    }

    getBuildString() {
        return new Promise<string>(resolve => {
            resolve('unknown (mock)')
        })
    }

    shutdown(restart: boolean) {
        this.dbg.log("(mock) cannot shutdown")
        return new Promise<boolean>(resolve => resolve(false))
    }

    setTime(now: moment.MomentInput) {
        const s = moment(now).format("D MMM YYYY HH:mm:ss").toUpperCase()
        const cmd = `date -s "${s}" && hwclock -w`
        this.dbg.log("(mock) setTime. command would be", cmd)
        return new Promise<boolean>(resolve => resolve(false))
    }

} 