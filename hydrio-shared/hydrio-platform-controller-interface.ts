import { MomentInput } from 'moment';
export interface IHydrioPlatformController {
    shutdown(restart: boolean): Promise<boolean>
    setTime(now: MomentInput): Promise<boolean>
    getBuildString(): Promise<string>
}