import * as moment from 'moment'
import { HPeriod, HItem } from '.'

export class HPItem extends HPeriod {

    static fromData(data: HPItem) {
        return data && new HPItem(data.item, data.start, data.end)
    }

    constructor(
        public item: HItem,
        start: moment.MomentInput,
        end: moment.MomentInput
    ) {
        super(start, end)
    }

    get startsInSeconds() {
        return this.startsIn.asSeconds()
    }

    toString(onlyTime = false) {
        return this.item.fullName
            + ' ' + (onlyTime ? this.start.format('HH:mm:ss') : this.start.toLocaleString())
            + ' ' + (onlyTime ? this.end.format('HH:mm:ss') : this.end.toLocaleString())
    }
}