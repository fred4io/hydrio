import { MomentInput } from 'moment'

export class HStateChange {
    constructor(
        public address: string,
        public newState: boolean,
        public isMaster: boolean,
        public timestamp: MomentInput
    ) { }
}