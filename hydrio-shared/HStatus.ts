import * as moment from 'moment'
import { HPItem } from '.'

export class HStatus {
    static fromData(data: HStatus) {
        return new HStatus(
            data.ready,
            data.disposed,
            data.isLocked,
            data.sequenceId,
            data.started == undefined ? undefined : moment(data.started),
            data.curCycle,
            HPItem.fromData(data.curItem),
            HPItem.fromData(data.nextItem),
            data.time == undefined ? undefined : moment(data.time)
        )
    }
    constructor(
        public ready: boolean,
        public disposed: boolean,
        public isLocked?: boolean,
        public sequenceId?: number,
        public started?: moment.Moment,
        public curCycle?: number,
        public curItem?: HPItem,
        public nextItem?: HPItem,
        public time?: moment.Moment
    ) { }
}