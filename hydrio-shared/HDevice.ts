
export class HDevice {

    public state: boolean

    public get isValidForSequence() {
        return !!this.address && !this.isDead && !this.isReserved
    }

    constructor(
        public id: number,
        public name: string,
        public address: string,
        public isDead = false,
        public isReserved = false,
    ) {

    }

    public copy(o: HDevice) {
        this.id = o.id
        this.name = o.name
        this.address = o.address
        this.isDead = o.isDead
        this.isReserved = o.isReserved
    }

    public static fromData(o: HDevice) {
        return new HDevice(o.id, o.name, o.address, !!o.isDead, !!o.isReserved)
    }
}