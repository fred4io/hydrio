import { HUtils } from '.'

export class I2cBit {

    constructor(
        public address: number,
        public register: number,
        public bitIndex: number
    ) { }

    isOn(byte: number) {
        const mask = 1 << this.bitIndex
        const isOn = (byte & mask) != 0
        return isOn
    }

    update(byte: number, isOn: boolean) {
        const mask = 1 << this.bitIndex
        let newByte: number
        if (isOn) {
            newByte = byte | mask
        } else {
            newByte = byte & ~mask
        }
        return newByte
    }

    toString() {
        return HUtils.hexString(this.address)
            + '/' + HUtils.hexString(this.register)
            + '/' + HUtils.bitIndexString(this.bitIndex)
    }
}