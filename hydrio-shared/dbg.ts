import * as debug from 'debug'
export interface IDbgOptions {
    notry?: boolean
}
export class Dbg {

    private debug: debug.IDebugger

    constructor(
        namespace: string, 
        public options: IDbgOptions = { notry: true }, 
        ...args
    ) {
        this.debug = debug(namespace)
        this.options = options
        this.log(namespace, ...args)
    }

    log(msg, ...args) {
        this.debug(msg, ...args)
    }
    try(funcName: string, ...args) {
        if (!this.options.notry) {
            this.debug(funcName, 'try', ...args)
        }
    }
    done(funcName: string, ...args) {
        this.debug(funcName, 'done', ...args)
    }
    error(funcName: string, ...args) {
        this.debug(funcName, 'error', ...args)
    }
}