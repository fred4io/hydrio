import { MomentInput, Duration } from 'moment'
import {
    Dbg, IHydrioDb,
    HSequenceGenerateInfo, HSequence, HItem, HDevice, HStatus, HParams,
    HydrioDeviceController, HydrioSequencer, IHydrioHardwareController,
    IHydrioPlatformController, HSystemInfo, HPowerLoss
} from '.'

export class HydrioControllerException extends Error { }
export class HydrioControllerNotReadyException extends HydrioControllerException {
    constructor() {
        super("controller not ready")
    }
}
export class HydrioControllerDisposedException extends HydrioControllerException {
    constructor() {
        super("controller disposed")
    }
}
export class HydrioControllerActiveSequenceLockedException extends HydrioControllerException {
    constructor(public activeSequenceId: number) {
        super("Active sequence lock is on. Cannot change active sequence")
    }
}
export class HydrioControllerActiveSequenceException extends HydrioControllerException {
    constructor(public activeSequenceId: number) {
        super("A sequence is active. Cannot change devices")
    }
}
export class HydrioControllerSequenceActiveException extends HydrioControllerException {
    constructor(public activeSequenceId: number) {
        super("Sequence cannot be modified while active")
    }
}

export class HydrioController {
    private dbg: Dbg
    private dc: HydrioDeviceController
    private sr: HydrioSequencer
    private ready = false
    private buildString: string
    private disposed = false
    private devices: HDevice[]
    private isLocked: boolean
    private activeSequenceId: number
    private masterDeviceId: number

    constructor(
        private hpc: IHydrioPlatformController,
        private hhc: IHydrioHardwareController,
        private db: IHydrioDb
    ) {
        this.dbg = new Dbg(this.constructor.name)
    }

    start() {
        this.dbg.try('start')
        return this.hhc.start(
            HParams.powerLossDetection ? () => this.onPowerLoss() : undefined,
            HParams.logI2cReadWrite
        ).then(() => this.hpc.getBuildString()
        ).then(buildString => {
            this.buildString = buildString
            this.dbg.log('buildDate', this.buildString)
        }).then(() => {
            this.dc = new HydrioDeviceController(this.hhc)
            this.devices = this.dc.getDevices()
            this.db.initData(this.devices)
            const powerLoss = this.db.getPowerLost()
            this.db.setPowerRecovered()
            this.masterDeviceId = this.db.getMasterDeviceId()
            this.dc.init(
                (deviceId, newState, isMaster) =>
                    this.db.setStateChange(deviceId, newState, isMaster),
                this.db.getSingleActive(),
                this.masterDeviceId
            )
            this.sr = new HydrioSequencer(
                (device, newState, masterState) =>
                    this.sequencerOnStateChange(device, newState, masterState)
            )
            this.activateSequence(this.db.getActiveSequenceId(), true, powerLoss)
            this.ready = true
            this.dbg.done('start')
        }).catch(err => {
            this.dbg.error('start', err)
        })
    }

    dispose() {
        if (this.disposed) { return }
        this.disposed = true
        this.dbg.log('disposing')
        this.sr.stop()
        this.dc.setAllOff(undefined, true)
        this.dc.dispose()
        this.hhc.dispose()
        this.db.close()
        this.dbg.log('disposed')
    }

    //#region API
    shutdown(restart: boolean) {
        this.checkReady()
        this.dbg.log('shutdown, restart', restart)
        this.dispose()
        return this.hpc.shutdown(restart)
    }
    setSystemTime(now: MomentInput) {
        this.checkReady()
        this.dbg.log('setSystemTime')
        return this.hpc.setTime(now)
    }

    simulatePowerLoss() {
        this.checkReady()
        this.dbg.log('simulatePowerLoss')
        this.hhc.simulatePowerLoss()
    }

    getSystemInfo() {
        return new HSystemInfo(
            this.buildString,
            this.hhc.boardRevision,
            this.hhc.i2cEnabled,
            this.hhc.pldEnabled
        )
    }
    getStatus() {
        if (!this.ready || this.disposed) {
            return new HStatus(this.ready, this.disposed)
        }
        const st = this.sr.getStatus()
        st.isLocked = this.isLocked
        return st
    }

    getIsLocked() {
        this.checkReady()
        if (this.isLocked == undefined) {
            this.isLocked = this.db.getIsLocked()
        }
        return this.isLocked
    }
    setIsLocked(locked: boolean) {
        this.checkReady()
        this.db.setIsLocked(locked)
        this.isLocked = !!locked
    }

    getSingleActive() {
        this.checkReady()
        return this.db.getSingleActive()
    }
    setSingleActive(singleActive: boolean) {
        this.checkReady()
        this.checkActiveSequence()
        this.db.setSingleActive(singleActive)
        this.dc.setSingleActive(singleActive)
    }

    getMasterDeviceId() {
        this.checkReady()
        if (this.masterDeviceId == undefined) {
            this.masterDeviceId = this.db.getMasterDeviceId()
        }
        return this.masterDeviceId
    }
    setMasterDeviceId(deviceId: number) {
        this.checkReady()
        this.checkActiveSequence()
        this.db.setMasterDeviceId(deviceId)
        this.dc.setMaster(deviceId)
        this.masterDeviceId = deviceId
    }
    getDevices() {
        this.checkReady()
        return this.db.getDevices()
    }
    setDeviceDead(deviceId: number, dead: boolean) {
        this.checkReady()
        this.checkActiveSequence()
        this.db.setDeviceDead(deviceId, !!dead)
        this.dc.setDead(deviceId, !!dead)
    }

    getState(deviceId: number) {
        this.checkReady()
        return this.dc.getState(deviceId)
    }
    setState(deviceId: number, state: boolean) {
        this.checkReady()
        this.checkActiveSequence()
        this.dc.setState(deviceId, state)
    }
    getAllStates() {
        this.checkReady()
        return this.dc.getAllStates()
    }

    getActiveSequenceId() {
        this.checkReady()
        return this.activeSequenceId
    }
    setActiveSequenceId(sequenceId: number) {
        this.checkReady()
        if (sequenceId == this.activeSequenceId) { return }
        if (this.isLocked) { throw new HydrioControllerActiveSequenceLockedException(sequenceId) }
        this.db.setActiveSequenceId(sequenceId)
        this.activateSequence(sequenceId)
    }

    getSequences() {
        this.checkReady()
        return this.db.getSequences()
    }
    createSequence(info: HSequenceGenerateInfo) {
        this.checkReady()
        return this.db.createSequence(info)
    }
    updateSequence(seq: HSequence) {
        this.checkReady()
        this.checkSequenceActive(seq && seq.id)
        this.db.updateSequence(seq)
    }
    deleteSequence(sequenceId: number) {
        this.checkReady()
        this.checkSequenceActive(sequenceId)
        this.db.deleteSequence(sequenceId)
    }
    getSequenceItems(sequenceId: number) {
        this.checkReady()
        return this.db.getSequenceItems(sequenceId)
    }
    createSequenceItem(item: HItem) {
        this.checkReady()
        this.checkSequenceActive(item && item.sequenceId)
        const itemId = this.db.createSequenceItem(item)
        this.dbg.log('createSequenceItem: ', item.id + '->' + itemId)
        item.id = itemId
        return item
    }
    updateSequenceItem(item: HItem) {
        this.checkReady()
        //this.checkSequenceActive(item && item.sequenceId)
        if (item.sequenceId == this.activeSequenceId) {
            this.db.updateActiveSequenceItem(item)
        } else {
            this.db.updateSequenceItem(item)
        }
    }
    deleteSequenceItem(itemId: number) {
        this.checkReady()
        if (itemId != undefined) {
            const seqId = this.db.getSequenceIdFromItemId(itemId)
            this.checkSequenceActive(seqId)
        }
        this.db.deleteSequenceItem(itemId)
    }

    //#endregion API

    private onPowerLoss() {
        this.dbg.log('power lost. stopping the system.')
        this.db.setPowerLost(this.sr.activeSequenceLastStarted)
        this.dispose()
        this.hpc.shutdown(false)
    }

    private activateSequence(sequenceId: number, isControllerStart = false, pli?: HPowerLoss ) {
        this.dbg.log('activateSequence', sequenceId)
        const curActiveSequenceId = this.activeSequenceId
        if (sequenceId == curActiveSequenceId) { return }
        this.activeSequenceId = sequenceId
        if (sequenceId == undefined && curActiveSequenceId != undefined) {
            this.sr.stop()
        } else if (sequenceId != undefined && sequenceId != curActiveSequenceId) {
            const seq = this.db.getSequence(sequenceId, true)
            seq.items.forEach(it => {
                it.device = this.devices.find(d => d.id == it.deviceId)
                if (it.device) {
                    it.device.isReserved = this.isDeviceReserved(it.device)
                }
            })
            this.sr.start(seq, isControllerStart, pli)
        }
    }

    private sequencerOnStateChange(device: HDevice, newState: boolean, masterState?: boolean) {
        this.dbg.log('sequencerOnStateChange', device && device.address, newState, masterState)
        if (masterState != undefined && this.masterDeviceId) {
            this.dc.setState(this.masterDeviceId, masterState)
        }
        this.dc.setState(device && device.id, newState)
    }

    private isDeviceReserved(device: HDevice) {
        return device && this.dc.isMaster(device.id)
    }

    private checkReady() {
        if (!this.ready) {
            throw new HydrioControllerNotReadyException()
        } else if (this.disposed) {
            throw new HydrioControllerDisposedException()
        }
    }
    private checkSequenceActive(sequenceId) {
        if (sequenceId != undefined && sequenceId == this.activeSequenceId) {
            throw new HydrioControllerSequenceActiveException(sequenceId)
        }
    }
    private checkActiveSequence() {
        if (this.activeSequenceId != undefined) {
            throw new HydrioControllerActiveSequenceException(this.activeSequenceId)
        }
    }
}