import { WebAPI } from "lib/web-api"
import { HydrioController, HSequenceGenerateInfo, HItem } from '@shared'

export class HydrioWebAPI extends WebAPI {

    constructor(private hc: HydrioController) {
        super()

        //const guardLockedSet = this.redirectIf(() => this.hc.getIsLocked(), { set: true })
        //const guardLockedAll = this.redirectIf(() => this.hc.getIsLocked(), { all: true })
        //const guardActiveSeq = this.redirectIf(req => this.hc.getActiveSequenceId() == req.body.id, { set: true })

        this.setupForData('testconnection', {
            get: () => true
        })
        this.setupForData('systeminfo', {
            get: () => this.hc.getSystemInfo()
        })
        this.setupForData('status', {
            get: () => this.hc.getStatus()
        })
        this.setupForData('shutdown', {
            post: req => this.hc.shutdown(req.body.restart)
        })
        this.setupForData('simulatepowerloss', {
            post: () => this.hc.simulatePowerLoss()
        })
        this.setupForData('setsystemtime', {
            post: req => this.hc.setSystemTime(req.body.now)
        })
        this.setupForData('islocked', {
            get: () => this.hc.getIsLocked(),
            put: req => this.hc.setIsLocked(!!req.body.locked)
        })
        this.setupForData('singleactive', {
            get: () => this.hc.getSingleActive(),
            //guard: guardLockedSet,
            put: req => this.hc.setSingleActive(!!req.body.singleActive)
        })

        this.setupForData('masterdeviceid', {
            get: () => this.hc.getMasterDeviceId(),
            //guard: guardLockedSet,
            put: req => this.hc.setMasterDeviceId(req.body.id)
        })
        this.setupForData('device', {
            //guard: guardLockedAll,
            get: () => this.hc.getDevices()
        })
        this.setupForItem('devicedead', {
            //guard: guardLockedSet,
            put: req => this.hc.setDeviceDead(req.params.id, !!req.body.isDead)
        })
        this.setupForItem('devicestate', {
            get: req => this.hc.getState(req.params.id),
            //guard: guardLockedSet,
            put: req => this.hc.setState(req.params.id, !!req.body.state)
        })
        this.setupForData('devicestates', {
            get: () => this.hc.getAllStates()
        })


        this.setupForData('sequence', {
            get: () => this.hc.getSequences(),
            post: req => this.hc.createSequence(req.body as HSequenceGenerateInfo)
        })
        this.setupForData('activesequenceid', {
            get: () => this.hc.getActiveSequenceId(),
            //guard: guardLockedSet,
            put: req => { this.hc.setActiveSequenceId(req.body.id) }
        })

        this.setupForItem('sequence', {
            //guard: guardActiveSeq,
            put: req => this.hc.updateSequence(req.body),
            delete: req => this.hc.deleteSequence(req.params.id)
        })
        this.setupForItem('sequenceitems', {
            get: req => this.hc.getSequenceItems(req.params.id)
        })
        this.setupForItem('sequenceitem', {
            //guard: guardActiveSeq,
            put: req => this.hc.updateSequenceItem(req.body),
            delete: req => this.hc.deleteSequenceItem(req.params.id),
        })
        this.setupForData('sequenceitem', {
            //guard: guardActiveSeq,
            post: req => this.hc.createSequenceItem(HItem.fromData(req.body))
        })
    }




}