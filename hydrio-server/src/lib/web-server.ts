
import * as http from 'http'
import * as express from 'express'
import * as debug from 'debug'

export class WebServerError {
    constructor(
        public error: NodeJS.ErrnoException,
        public message: string,
        public fatal: boolean
    ) { }
    public toString() {
        return (this.fatal ? '(fatal) ' : '') + `${this.error} - ${this.message}`
    }
}

export class WebServer {

    public httpServer: http.Server
    private port: any
    public onError: (error: WebServerError) => void
    private dbg: debug.IDebugger

    constructor(private app: express.Application) {
        this.dbg = debug(this.constructor.name)

        this.port = WebServer.normalizePort(process.env.PORT || 3000)
        this.app.set('port', this.port)

        this.httpServer = http.createServer(this.app)
        this.httpServer.on('error', error => this.handleError(error))
        this.httpServer.on('listening', () => this.onListening())
        this.httpServer.listen(this.port)
    }

    private static normalizePort(val: number | string): number | string | boolean {
        let port: number = (typeof val === 'string') ? parseInt(val, 10) : val;
        if (isNaN(port)) return val;
        else if (port >= 0) return port;
        else return false;
    }

    private handleError(error: NodeJS.ErrnoException): void {
        if (error.syscall !== 'listen') {
            if (this.onError) {
                this.onError(new WebServerError(error, 'unknown', false))
            } else {
                throw error
            }
        }

        const port = this.port
        let bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port
        let msg: string
        switch (error.code) {
            case 'EACCES':
                msg = `${bind} requires elevated privileges`
                if (this.onError) {
                    this.onError(new WebServerError(error, msg, true))
                } else {
                    this.dbg(msg)
                    process.exit(1)
                }
                break

            case 'EADDRINUSE':
                msg = `${bind} is already in use`
                if (this.onError) {
                    this.onError(new WebServerError(error, msg, true))
                } else {
                    this.dbg(msg)
                    process.exit(1)
                }
                break

            default:
                if (this.onError) {
                    this.onError(new WebServerError(error, 'default', false))
                } else {
                    throw error
                }
        }
    }

    private onListening(): void {
        let addr = this.httpServer.address()
        let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`
        this.dbg(`Listening on ${bind}`)
    }
}



