import { Router, Request, Response, NextFunction } from 'express'
import * as debug from 'debug'

export type ReqRes = (req: Request, res?: Response) => any
export type GuardOptions = {
    /** get, put, post, delete */ all?: boolean,
    /** put, post, delete */ set?: boolean,
    get?: boolean,
    put?: boolean,
    post?: boolean,
    delete?: boolean
}
export type Guard = GuardOptions & {
    redirect: (req?: Request) => string,
    status?: number
}

export interface IRouteData {
    get?: ReqRes,
    put?: ReqRes,
    post?: ReqRes,
    guard?: Guard,
}
export interface IRouteItem {
    get?: ReqRes,
    put?: ReqRes,
    delete?: ReqRes
    guard?: Guard,
}

export class WebAPI {

    public router: Router
    private dbg: debug.IDebugger

    constructor() {
        this.dbg = debug(this.constructor.name)
        this.router = Router()
    }

    /** setup routes for a simple url */
    setupForData(url: string, options: IRouteData) {
        url = '/' + url
        if (options.get) {
            this.router.route(url)
                .get((req, res, next) => {
                    this.redirectOrExec(options,
                        guard => guard.all || guard.get,
                        options.get,
                        req, res, next)
                })
        }
        if (options.put) {
            this.router.route(url)
                .put((req, res, next) => {
                    this.redirectOrExec(options,
                        guard => guard.all || guard.set || guard.put,
                        options.put,
                        req, res, next)
                })
        }
        if (options.post) {
            this.router.route(url)
                .post((req, res, next) => {
                    this.redirectOrExec(options,
                        guard => guard.all || guard.set || guard.post,
                        options.post,
                        req, res, next)
                })
        }
    }

    /** setup routes for and url with an id */
    setupForItem(url: string, options: IRouteItem) {
        url = '/' + url + '/:id'
        if (options.get) {
            this.router.route(url)
                .get((req, res, next) => {
                    this.redirectOrExec(options,
                        guard => guard.all || guard.get,
                        options.get,
                        req, res, next)
                })
        }
        if (options.put) {
            this.router.route(url)
                .put((req, res, next) => {
                    this.redirectOrExec(options,
                        guard => guard.all || guard.set || guard.put,
                        options.put,
                        req, res, next)
                })
        }
        if (options.delete) {
            this.router.route(url)
                .delete((req, res, next) => {
                    this.redirectOrExec(options,
                        guard => guard.all || guard.set || guard.delete,
                        options.delete,
                        req, res, next)
                })
        }
    }

    /** returns a guard for redirecting upon specified condition and verbs */
    redirectIf(
        test: (req: Request) => boolean, 
        opt: GuardOptions = { all: true }, 
        redirectTo = '/', 
        /** 409: Conflict */ status = 409
    ) {
        return {
            redirect: req => test(req) ? redirectTo : undefined,
            status: status,
            all: opt.all, set: opt.set,
            get: opt.get, put: opt.put, post: opt.post, delete: opt.delete
        } as Guard
    }

    private redirectOrExec(
        options: IRouteItem | IRouteData,
        guardIf: (guardOptions: GuardOptions) => boolean,
        exec: (req: Request) => any,
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        const redirectTo =
            options && options.guard && options.guard.redirect
            && guardIf && guardIf(options.guard)
            && options.guard.redirect(req)
        // console.log(options, redirectTo)
        if (redirectTo) {
            res.redirect(redirectTo, options.guard.status || 409) //Conflict
            this.log(req, res)
        } else { this.exec(exec, req, res, next) }
    }

    private exec(func: (req: Request) => any, req: Request, res: Response, next: NextFunction) {
        try {
            const data = func(req)
            res.json(data)
        }
        catch (err) { res.status(500); next(err) }
        finally { this.log(req, res) }
    }

    private log(req: Request, res?: Response, context?: string) {
        this.dbg(req.method + ' ' + req.url
            + (!res ? '' : (' ' + res.statusCode + ' ' + res.statusMessage))
            + (context == undefined ? '' : context)
        )
    }

}