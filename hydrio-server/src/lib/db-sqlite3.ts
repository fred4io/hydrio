import * as Database from 'better-sqlite3'
import * as Integer from 'integer'
import { Dbg } from '@shared'

interface RunResult {
    changes: number
    lastInsertROWID: Integer.IntLike
}
interface Statement {
    run: () => void
}

export class DbSqlite3 {
    private dbg: Dbg
    private db: Database
    private begin: Statement
    private commit: Statement
    private rollback: Statement

    constructor(private dbFileName: string) {
        this.dbg = new Dbg(this.constructor.name, undefined, dbFileName)
        this.db = new Database(this.dbFileName)
        this.begin = this.db.prepare('BEGIN')
        this.commit = this.db.prepare('COMMIT')
        this.rollback = this.db.prepare('ROLLBACK')
    }

    close() {
        this.db.close()
    }

    exec(logId: string, sql: string, params?: any[]) {
        const funcName = 'exec'
        this.dbg.try(funcName, logId)
        try {
            this.db.exec(sql)
            this.dbg.done(funcName, logId)
        }
        catch (err) {
            this.dbg.error(funcName, logId, sql, err)
            throw err
        }
    }

    runOne(logId: string, sql: string, params?: any[]) {
        const funcName = 'runOne'
        this.dbg.try(funcName, logId)
        try {
            var stm = this.db.prepare(sql)
            var res = stm.run(params || [])
            this.dbg.done(funcName, logId, res.changes)
            return res.lastInsertROWID as number
        }
        catch (err) {
            this.dbg.error(funcName, logId, params, sql, err)
            throw err
        }
    }

    runMany(logId: string, sql: string, params: any[][]) {
        const funcName = 'runMany'
        this.dbg.try(funcName, logId)
        try {
            var stm = this.db.prepare(sql)
            var results: RunResult[] = []
            params.forEach((p, i) => {
                try {
                    var res = stm.run(p)
                    results.push(res)
                }
                catch (err) {
                    this.dbg.error(funcName, logId, i, p, sql, err)
                    throw err;
                }
            })
            this.dbg.done(funcName, logId, results.length)
            return results.map(r => r.lastInsertROWID)
        }
        catch (err) {
            this.dbg.error(funcName, logId, err)
            throw err
        }
    }

    getAll<T>(logId: string, sql: string, withRow: (row: any, index?: number) => T, params?: any[]) {
        const funcName = 'getAll'
        try {
            this.dbg.try(funcName, logId)
            var stm = this.db.prepare(sql)
            var rows = stm.all(params || [])
            try {
                var res = rows.map(withRow)
                this.dbg.done(funcName, logId, res.length)
                return res
            }
            catch (err) {
                this.dbg.error(funcName, logId, 'before map', rows.length)
                throw err
            }
        }
        catch (err) {
            this.dbg.error(funcName, logId, params, sql, err)
            throw err
        }
    }

    getOne<T>(logId: string, sql: string, withRow: (row: any, index?: number) => T, params?: any[]) {
        const funcName = 'getOne'
        try {
            this.dbg.try(funcName, logId)
            var stm = this.db.prepare(sql)
            var row = stm.get(params || [])
            try {
                var res = row ? withRow(row) : undefined
                this.dbg.done(funcName, logId, res ? '' : 'nodata')
                return res
            }
            catch (err) {
                this.dbg.error(funcName, logId, 'before map')
                throw err
            }
        }
        catch (err) {
            this.dbg.error(funcName, logId, params, sql, err)
            throw err
        }
    }

    getScalar<T>(logId: string, sql: string, params?: any[]) {
        const funcName = 'getScalar'
        try {
            this.dbg.try(funcName, logId)
            var stm = this.db.prepare(sql)
            stm.pluck(true)
            var res = stm.get(params || [])
            this.dbg.done(funcName, logId, res)
            return res as T
        } catch (err) {
            this.dbg.error(funcName, logId, params, sql, err)
            throw err
        }
    }

    asTransaction(func: (...args) => void, logId: string) {
        logId = 'transaction ' + logId
        return (...args) => {
            this.begin.run()
            this.dbg.log(logId, 'begun')
            try {
                func(...args)
                this.commit.run()
                this.dbg.log(logId, 'commited')
            } finally {
                if (this.db.inTransaction) {
                    this.rollback.run()
                    this.dbg.log(logId, 'rolledback')
                }
            }
        }
    }
}