import { WebServer } from './web-server'
import { ExpressApp } from './express-app'
import { WebAPI } from './web-api'
import { Dbg } from '@shared'

export abstract class ServerBase {

    protected dbg: Dbg
    private expressApp: ExpressApp
    private webServer: WebServer
    private webApi: WebAPI

    constructor() {
        this.dbg = new Dbg(this.constructor.name)
        this.prepareExit()
        this.run()
    }

    protected abstract start(): Promise<WebAPI>
    protected onError(err: any) { }
    protected dispose() { }

    private run() {
        const logId = 'run'
        this.start()
            .then(api => {
                this.dbg.try(logId)
                this.webApi = api
                this.expressApp = new ExpressApp(this.webApi.router, true, false, false)
                this.webServer = new WebServer(this.expressApp.express)
                this.webServer.onError = err => {
                    this.dbg.error(logId, err)
                    this.onError(err)
                }
                this.dbg.done(logId)
            })
            .catch(err => {
                this.dbg.error(logId, err)
                throw err
            })
    }

    private prepareExit() {
        // so the program will not close instantly
        process.stdin.resume()

        const self = this
        function exitHandler(err) {
            if (err) { this.dbg.log('fatal error', err.stack || err) }
            try {
                if (typeof self.dispose == 'function') {
                    self.dispose()
                }
            }
            catch (err) { this.dbg.log('dispose error', err.stack || err) }
            finally { process.exit() }
        }

        // do something when app is closing
        process.on('exit', exitHandler.bind(this))

        // catches ctrl+c event
        process.on('SIGINT', exitHandler.bind(this))

        // catches "kill pid" (for example: nodemon restart)
        process.on('SIGUSR1', exitHandler.bind(this))
        process.on('SIGUSR2', exitHandler.bind(this))

        // catches uncaught exceptions
        process.on('uncaughtException', exitHandler.bind(this))
    }
}
