import * as express from 'express'
import * as bodyParser from 'body-parser';
import * as path from 'path'
import * as appRoot from 'app-root-path';
import { Dbg } from '@shared';

export class ExpressApp {

    public express: express.Application
    private dbg: Dbg

    constructor(
        private apiRouter: express.Router,
        private redirectNotFoundToPublicIndex = true,
        private rootWelcomeMessage: string | false = "Hello there!",
        private apiRootMessage: string | false = "Api works!"
    ) {
        this.dbg = new Dbg(this.constructor.name)
        this.init()
    }

    private init() {
        this.dbg.try('init')
        try {
            this.express = express()
            this.middleware()
            this.routes()
            this.dbg.done('init')
        }
        catch (err) {
            this.dbg.error('init', err)
            throw err
        }
    }

    // Configure Express middleware.
    private middleware(): void {
        // this.express.use(logger('dev'));

        // support application/json type post data
        this.express.use(bodyParser.json())

        //support application/x-www-form-urlencoded post data
        this.express.use(bodyParser.urlencoded({ extended: false }))
    }

    private log(req: express.Request, res?: express.Response) {
        this.dbg.log(req.method + ' ' + req.url
            + (res ? (' ' + res.statusCode + ' ' + res.statusMessage) : ''))
    }

    // Configure API endpoints.
    private routes(): void {

        // Point static path to dist
        this.express.use(express.static(path.join(appRoot.path, 'public')))

        // Set our api routes
        this.express.use('/api', this.apiRouter)

        if (this.apiRootMessage) {
            // set default api routes
            this.express.use(['/api$/', '/api/$/'], (req, res) => {
                try { res.send(this.apiRootMessage) }
                finally { this.log(req, res) }
            })
        }

        if (this.rootWelcomeMessage) {
            // Set / route
            this.express.use('/$/', (req, res) => {
                try { res.send(this.rootWelcomeMessage) }
                finally { this.log(req, res) }
            })
        }

        // Catch all other routes
        if (this.redirectNotFoundToPublicIndex) {
            // and return the index file
            this.express.get('*', (req, res) => {
                try { res.sendFile(path.join(appRoot.path, 'public/index.html')) }
                finally { this.log(req, res) }
            })
        } else {
            // or send 404 not found
            this.express.get('*', (req, res) => {
                try { res.sendStatus(404) }
                finally { this.log(req, res) }
            })
        }
    }

}
