import * as moment from 'moment'
import { DbSqlite3 } from 'lib/db-sqlite3'
import {
    Dbg, IHydrioDb, HDevice, HSequence, HItem, HSequenceGenerateInfo,
    HParams, HUtils, HStateChange, HPowerLoss
} from '@shared'

export class HydrioDBSqlite implements IHydrioDb {
    private dbg: Dbg
    private db: DbSqlite3

    constructor(private dbFilePath = './data/hydrio.db.sqlite3') {
        this.dbg = new Dbg(this.constructor.name, undefined, dbFilePath)
        this.db = new DbSqlite3(this.dbFilePath)
    }

    close() {
        this.db.close()
    }

    initData(devices: HDevice[], reset = false) {
        this.dbg.try('initData')
        try {
            if (reset) {
                ['Device', 'Param', 'Sequence', 'SequenceItem', 'StateChange', 'PowerLoss']
                    .forEach(t => {
                        this.db.runOne('drop-table/' + t,
                            "DROP TABLE IF EXISTS `" + t + "`"
                        )
                    })
            }
            this.db.exec('db-init-tables',
                "CREATE TABLE IF NOT EXISTS `Param` (\
                    `name`	TEXT NOT NULL PRIMARY KEY,\
                    `value`	TEXT\
                );\
                CREATE TABLE IF NOT EXISTS `Device` (\
                    `id`	INTEGER NOT NULL PRIMARY KEY,\
                    `name`	TEXT NOT NULL,\
                    `address`	TEXT NOT NULL,\
                    `dead`	BOOLEAN NOT NULL DEFAULT 0\
                );\
                CREATE TABLE IF NOT EXISTS `Sequence` (\
                    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
                    `name`	TEXT NOT NULL,\
                    `startTime`	TEXT,\
                    `repeat`    INTEGER NOT NULL DEFAULT 1\
                );\
                CREATE TABLE IF NOT EXISTS `SequenceItem` (\
                    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
                    `name`	TEXT NOT NULL,\
                    `deviceId`    INTEGER,\
                    `durationSeconds`	INTEGER,\
                    `info`    TEXT,\
                    `position`	INTEGER,\
                    `sequenceId`    INTEGER NOT NULL\
                );\
                CREATE TABLE IF NOT EXISTS `StateChange` (\
                    `deviceId`  INTEGER NOT NULL,\
                    `state` BOOLEAN NOT NULL,\
                    `master`	BOOLEAN NOT NULL,\
                    `timestamp`	INTEGER DEFAULT (strftime('%s', 'now')) \
                );\
                CREATE TABLE IF NOT EXISTS `PowerLoss` (\
                    `powerLostOn`   INTEGER DEFAULT (strftime('%s', 'now')), \
                    `seqStartedOn`  INTEGER, \
                    `recoveredOn`   INTEGER\
                );\
                "
            )

            this.setDefaultParam('master-device-id', HParams.defaultMasterDeviceId)
            this.setDefaultParam('active-sequence-id', null)
            this.setDefaultParam('is-locked', false)
            this.setDefaultParam('single-active', true)

            if (this.db.getScalar<number>(
                'count-device',
                "SELECT COUNT(1) FROM Device"
            ) == 0) {
                this.db.runMany(
                    'db-populate-device',
                    "INSERT INTO Device (id, name, address, dead) VALUES (?,?,?,?)",
                    devices.map(o => [o.id, o.name, o.address, 0]))
            }

            this.dbg.done('init')
        }
        catch (err) {
            this.dbg.error('init', err)
            throw err
        }
    }

    //#region param
    getIsLocked() {
        return this.getParamBoolean('is-locked')
    }
    setIsLocked(locked: boolean) {
        locked = !!locked
        this.dbg.log('setIsLocked', locked)
        return this.setParam('is-locked', locked)
    }

    getSingleActive() {
        return this.getParamBoolean('single-active')
    }
    setSingleActive(singleActive: boolean) {
        singleActive = !!singleActive
        this.dbg.log('setSingleActive', singleActive)
        this.setParam('single-active', singleActive)
    }

    private getParam(name: string) {
        return this.db.getScalar<string>('getParam/' + name,
            "SELECT value FROM Param WHERE name=?",
            [name]
        )
    }
    private getParamNumber(name: string) {
        const res = this.getParam(name)
        return res ? parseInt(res, 10) : undefined
    }
    private getParamBoolean(name: string) {
        const res = this.getParam(name)
        return res ? res === 'true' : undefined
    }
    private setParam(name: string, value: number | boolean | string) {
        value = value == undefined ? null : value.toString()
        this.db.runOne('setParam/' + name + ':' + value,
            "REPLACE INTO Param (name,value) VALUES(?,?)",
            [name, value]
        )
        //this.dbg.log('test param ' + name, this.getParam(name))
    }

    private setDefaultParam(name: string, value: any) {
        value = value == undefined ? null : value.toString()
        this.db.runOne('setDefaultParam/' + name + ':' + value,
            "INSERT INTO Param (name, value)\
             SELECT ?,? WHERE NOT EXISTS(SELECT 1 FROM Param WHERE name=?)",
            [name, value, name]
        )
    }
    //#endregion param

    //#region device
    getMasterDeviceId() {
        return this.getParamNumber('master-device-id')
    }
    setMasterDeviceId(id: number) {
        this.dbg.log('setMasterDeviceId', id)
        return this.setParam('master-device-id', id)
    }

    getDevices() {
        return this.db.getAll(
            'getDevices',
            "SELECT * FROM Device",
            o => new HDevice(o.id, o.name, o.address, o.dead == 1)
        ).filter((_, i) => i < HParams.nbDevices)
    }
    setDeviceDead(id: number, isDead: boolean) {
        return this.db.runOne(
            'setDeviceDead/' + id + ':' + isDead,
            "UPDATE Device set dead=? WHERE id=?",
            [isDead ? 1 : 0, id]
        )
    }
    //#endregion device

    //#region sequence
    getActiveSequenceId() {
        return this.getParamNumber('active-sequence-id')
    }
    setActiveSequenceId(id: number) {
        this.dbg.log('setActiveSequenceId', id)
        return this.setParam('active-sequence-id', id)
    }

    getSequences() {
        var seqs = this.db.getAll(
            'getSequences',
            "SELECT * FROM Sequence",
            o => new HSequence(o.id, o.name, o.startTime, o.repeat)
        )
        return seqs
    }

    getSequence(seqId: number, withItems = false) {
        var seq = this.db.getOne(
            'getSequence/' + seqId,
            "SELECT * FROM Sequence WHERE id = ?",
            o => new HSequence(o.id, o.name, o.startTime, o.repeat),
            [seqId]
        )
        if (withItems) {
            seq.items = this.getSequenceItems(seq.id)
        }
        return seq
    }
    updateSequence(seq: HSequence) {
        this.db.runOne(
            'updateSequence',
            "UPDATE Sequence set name=?, startTime=?, repeat=? WHERE id=?",
            [seq.name, seq.startTime, seq.repeat, seq.id]
        )
    }
    createSequence(info: HSequenceGenerateInfo) {
        //this.dbg.log('createSequence', info)

        const seq = new HSequence(undefined, info.name)

        this.db.asTransaction(() => {
            seq.id = this.db.runOne(
                'createSequence',
                "INSERT INTO Sequence (name, startTime, repeat) VALUES (?,?,?)",
                [seq.name, seq.startTime, seq.repeat]
            )
            this.db.runMany(
                'createSequence-sequenceitems-' + seq.id,
                "INSERT INTO SequenceItem (sequenceId, name, deviceId, durationSeconds, position) values (?,?,?,?,?)",
                HUtils.generateSequenceItems(seq.id, info)
                    .map((it, i) => [seq.id, it.name, it.deviceId, it.durationSeconds, i])
            )
        }, 'createSequence/' + seq.name)()

        return seq
    }
    deleteSequence(seqId: number) {
        this.db.asTransaction(() => {
            this.db.runOne(
                'delete-sequence',
                "DELETE FROM Sequence WHERE id=?",
                [seqId]
            )
            this.db.runOne(
                'delete-sequence-items',
                "DELETE FROM SequenceItem WHERE sequenceId=?",
                [seqId]
            )
        }, 'deleteSequence/' + seqId)()
    }
    //#endregion sequence

    //#region sequence items
    getSequenceItems(seqId: number) {
        var items = this.db.getAll(
            'getSequenceItems/' + seqId,
            "SELECT * FROM SequenceItem WHERE sequenceId = ? ORDER BY position",
            o => new HItem(o.sequenceId, o.id, o.name, o.deviceId, o.durationSeconds, o.position, o.info),
            [seqId]
        )
        return items
    }
    getSequenceIdFromItemId(itemId: number) {
        return this.db.getScalar<number>(
            'getSequenceIdFromItemId/' + itemId,
            "SELECT SequenceId FROM SequenceItem WHERE id=?",
            [itemId]
        )
    }
    createSequenceItem(o: HItem) {
        o = HItem.fromData(o)
        return this.db.runOne(
            'addSequenceItem',
            "INSERT INTO SequenceItem (sequenceId, name, deviceId, durationSeconds, position) values (?,?,?,?,?)",
            [o.sequenceId, o.name, o.deviceId, o.durationSeconds, o.position]
        )
    }
    updateActiveSequenceItem(o: HItem) {
        return this.db.runOne(
            '-update-item',
            "UPDATE SequenceItem set name=?, info=? WHERE id=?",
            [o.name, o.info, o.id]
        )
    }
    updateSequenceItem(o: HItem) {
        o = HItem.fromData(o)
        const logId = 'updateSequenceItem/' + o.id
        let result = 0
        this.db.asTransaction(() => {
            const oldPosition = this.db.getScalar<number>(
                'get-item-position',
                "SELECT position FROM SequenceItem WHERE id=?",
                [o.id]
            )
            if (oldPosition == undefined) {
                throw new Error("item not found")
            }
            const newPosition = o.position
            this.dbg.log('updateSequenceItem', o.id, o.sequenceId, oldPosition + '->' + newPosition)
            if (newPosition > oldPosition) {
                result += this.db.runOne(
                    logId + '-move-up-others',
                    "UPDATE SequenceItem SET position=position-1 WHERE sequenceId=? AND position>? AND position<=?",
                    [o.sequenceId, oldPosition, newPosition]
                )
            } else if (newPosition < oldPosition) {
                result += this.db.runOne(
                    logId + '-move-down-others',
                    "UPDATE SequenceItem SET position=position+1 WHERE sequenceId=? AND position<? AND position>=?",
                    [o.sequenceId, oldPosition, newPosition]
                )
            }
            result += this.db.runOne(
                logId + '-update-item',
                "UPDATE SequenceItem set name=?, deviceId=?, durationSeconds=?, position=?, info=? WHERE id=?",
                [o.name, o.deviceId, o.durationSeconds, o.position, o.info, o.id]
            )
        }, logId)()
        return result
    }
    deleteSequenceItem(itemId: number) {
        let result: number
        const logId = 'deleteSequenceItem/' + itemId
        this.db.asTransaction(() => {
            const info =
                this.db.getOne<{ sequenceId: number, position: number }>(
                    logId + '-get-item-info',
                    "SELECT sequenceId, position FROM SequenceItem WHERE id=?",
                    row => row,
                    [itemId]
                )
            if (info) {
                result = this.db.runOne(
                    logId + '-delete-item',
                    "DELETE FROM SequenceItem WHERE id=?",
                    [itemId]
                )
                if (result >= 1) {
                    result += this.db.runOne(
                        logId + '-move-up-others',
                        "UPDATE SequenceItem SET position=position-1 WHERE sequenceId=? AND position>?",
                        [info.sequenceId, info.position]
                    )
                }
            }
        }, logId)()
        return result
    }
    //#endregion sequence items

    //#region state change
    setStateChange(deviceId: number, newState: boolean, isMaster: boolean) {
        return this.db.runOne('state-change',
            "INSERT INTO StateChange (deviceId, state, master) VALUES (?,?,?)",
            [deviceId, newState ? 1 : 0, isMaster ? 1 : 0]
        )
    }
    getStateChangeMinMaxTimeStamps() {
        return this.db.getOne(
            'StateChange-timestamp-minmax',
            "SELECT MIN(timestamp) as min, MAX(timestamp) as max FROM StateChange",
            row => ({
                min: row.min != undefined && parseInt(row.min, 10),
                max: row.max != undefined && parseInt(row.max, 10)
            })
        )
    }
    getStateChanges(from?: moment.MomentInput, to?: moment.MomentInput) {
        const start = moment(from), end = moment(to)
        let min: number, max: number
        if (start.isValid() && end.isValid()) {
            min = start.unix()
            max = end.unix()
        } else {
            const minMax = this.getStateChangeMinMaxTimeStamps()
            if (minMax.min == undefined) {
                return new Array<HStateChange>()
            }
            min = start.isValid() ? start.unix() : minMax.min
            max = end.isValid() ? end.unix() : minMax.max
        }
        return this.db.getAll('getStateChanges/' + min + ',' + max,
            "SELECT address, state, master, timestamp \
                FROM StateChange \
                WHERE timestamp >= ? AND timestamp <= ?",
            row => new HStateChange(row.address, row.state, row.master, row.timestamp),
            [min, max]
        )
    }
    //#endregion state change

    //#region powerloss
    setPowerLost(seqStartedOn: moment.Moment) {
        return this.db.runOne('setPowerLost',
            "INSERT INTO PowerLoss (seqStartedOn) VALUES (?)",
            [seqStartedOn && seqStartedOn.unix() || undefined]
        )
    }
    getPowerLost() {
        return this.db.getOne<HPowerLoss>('getPowerLost',
            "SELECT powerLostOn, seqStartedOn FROM PowerLoss WHERE recoveredOn IS NULL",
            row => row && row.powerLostOn ?
                new HPowerLoss(
                    moment.unix(row.powerLostOn),
                    row.seqStartedOn ? moment.unix(row.seqStartedOn) : undefined
                )
                : undefined
        )
    }
    setPowerRecovered() {
        return this.db.runOne('setPowerRecovered',
            "UPDATE PowerLoss set recoveredOn=strftime('%s', 'now') WHERE recoveredOn IS NULL"
        )
    }
    //#endregion powerloss
}