
import * as raspi from 'raspi'
import * as board from 'raspi-board'
import { I2C } from 'raspi-i2c'
import { PULL_DOWN, LOW, HIGH, DigitalInput } from 'raspi-gpio'
import { Dbg, IHydrioHardwareController, HParams, I2cData } from '@shared'

export class HydrioPiHardwareController implements IHydrioHardwareController {

    private noLog = false
    private dbg: Dbg
    private ready = false
    private disposed = false

    private i2c: I2C
    private i2cReady = false
    private pldReady = false
    private boardRev: string
    private initDone = false

    private pldPin: number | string = 'GPIO21'
    private pldPull = PULL_DOWN
    private onPowerLoss?: () => void
    private pldInput: DigitalInput

    constructor() {
        this.dbg = new Dbg(this.constructor.name)
    }

    get i2cEnabled() { return this.i2cReady }
    get pldEnabled() { return this.pldReady }
    get boardRevision() { return this.boardRev }

    start(
        onPowerLoss?: () => void,
        i2cLog = false
    ) {
        if (this.initDone) {
            this.dbg.log("already starded")
            return new Promise<void>(resolve => resolve())
        } else {
            this.noLog = !i2cLog
            this.dbg.log("start")
            this.onPowerLoss = onPowerLoss
            return this.initRaspi()
                .then(() => { this.initDone = true })
        }
    }

    dispose() {
        if (this.disposed) { return }
        this.disposed = true
        this.dbg.log('disposing')
        this.i2cAllOff(true)
        if (this.pldInput) { this.pldInput.destroy() }
        if (this.i2c) { this.i2c.destroy() }
        this.dbg.log('disposed')
    }

    simulatePowerLoss() {
        this.dbg.log('simulatePowerLoss')
        this.onPowerLossStateChange(HIGH)
    }

    i2cReadByte(address: number, register: number) {
        this.checkI2cReady()
        if (!this.noLog) { this.dbg.log('i2cReadByte', address, register) }
        return this.i2c.readByteSync(address, register)
    }
    i2cWriteByte(address: number, register: number, byte: number, noLog = false, force = false) {
        if (!force) { this.checkI2cReady() }
        if (!noLog && !this.noLog) { this.dbg.log('i2cWriteByte', address, register, byte) }
        try { this.i2c.writeByteSync(address, register, byte) }
        catch (err) { if (!force) { throw err } }
    }
    i2cAllOff(force = false) {
        if (!force) { this.checkI2cReady() }
        this.dbg.log('i2cAllOff')
        for (let i = 0; i < HParams.deviceNbI2cModules; i++) {
            for (let j = 0; j < HParams.deviceNbBanks; j++) {
                const adr = HParams.deviceI2cFirstAddress + i
                const reg = HParams.deviceI2cFirstRegister + j
                try { this.i2cWriteByte(adr, reg, 0, true, true) }
                catch (err) { if (!force) { throw err } }
            }
        }
    }
    i2cReadAll(force = false) {
        if (!force) { this.checkI2cReady() }
        const result = new Array<I2cData>()
        for (let i = 0; i < HParams.deviceNbI2cModules; i++) {
            for (let j = 0; j < HParams.deviceNbBanks; j++) {
                const adr = HParams.deviceI2cFirstAddress + i
                const reg = HParams.deviceI2cFirstRegister + j
                let byte: number = undefined
                try { byte = this.i2cReadByte(adr, reg) }
                catch (err) { if (!force) { throw err } }
                result.push(new I2cData(adr, reg, byte))
            }
        }
        return result
    }

    private checkI2cReady() {
        this.checkReady()
        if (!this.i2cReady) { throw new Error("i2c not ready") }
    }
    private checkReady() {
        if (!this.ready) { throw new Error("not ready") }
        else if (this.disposed) { throw new Error("disposed") }
    }


    private initRaspi() {
        return new Promise<void>(resolve => {
            this.dbg.log("init raspi")
            raspi.init(() => {
                this.dbg.log("board revision: " + board.getBoardRevision())
                this.setupPowerLossDetection()
                this.setupI2c()
                this.ready = true
                this.dbg.log('ready')
                resolve()
            })
        })
    }

    private setupI2c() {
        this.dbg.log('setting up I2C')
        try {
            this.i2c = new I2C()
            for (let j = 0; j < HParams.deviceNbI2cModules; j++) {
                for (let i = 0; i < HParams.deviceNbBanks; i++) {
                    this.i2cWriteByte(HParams.deviceI2cFirstAddress + j,
                        HParams.deviceI2cFirstInitRegister + i, 0, false, true)
                }
            }
            this.i2cReady = true
            this.dbg.log('I2C ready')
        }
        catch (err) { this.dbg.error('setupI2c', err) }
    }

    private setupPowerLossDetection() {
        if (this.onPowerLoss == undefined) {
            this.dbg.log('no powerloss detection')
        } else {
            this.dbg.log('setting up powerloss detection')
            try {
                this.pldInput = new DigitalInput({
                    pin: this.pldPin,
                    pullResistor: this.pldPull
                })

                let initDone = this.pldInput.value == LOW
                if (initDone) { this.dbg.log('powerloss detection ready') }
                this.pldInput.on('change', state => {
                    if (initDone) {
                        this.onPowerLossStateChange(state)
                    } else if (state == LOW) {
                        this.dbg.log('powerloss detection now ready')
                        initDone = true
                    }
                })
                this.pldReady = true
                this.dbg.log('powerloss detection started')
            }
            catch (err) {
                this.dbg.error('setupPowerLossDetection', err)
            }
        }
    }

    private onPowerLossStateChange(state: number) {
        if (!this.ready || this.disposed) { return }
        if (state == HIGH) {
            this.dbg.log('power lost')
            this.onPowerLoss()
        } else {
            this.dbg.log('power is back')
        }
    }

}