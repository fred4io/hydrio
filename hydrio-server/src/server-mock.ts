import { ServerBase } from 'lib/server-base'
import { HydrioDBSqlite } from 'hydrio-db-sqlite'
import { HydrioWebAPI } from 'hydrio-web-api'
import {
    HydrioController, HydrioHardwareControllerMock,
    HydrioPlatformControllerMock
} from '@shared'

export class HydrioServerMock extends ServerBase {

    private hc: HydrioController

    protected start() {
        this.hc = new HydrioController(
            new HydrioPlatformControllerMock(),
            new HydrioHardwareControllerMock(),
            new HydrioDBSqlite()
        )
        return this.hc.start().then(() => new HydrioWebAPI(this.hc))
    }

    dispose() {
        this.hc.dispose()
    }
}

export default new HydrioServerMock()
