
import { exec } from 'child_process'
import * as fs from 'fs'
import * as moment from 'moment'
import { Dbg, IHydrioPlatformController } from '@shared'

export class HydrioPiPlatformController implements IHydrioPlatformController{

    private dbg: Dbg

    constructor() {
        this.dbg = new Dbg(this.constructor.name)
    }

    setTime(now: moment.MomentInput) {
        if (moment().isSame(now, 's')) {
            this.dbg.log('setTime: no change')
            return new Promise<boolean>(resolve => resolve(false))
        }
        const s = moment(now).format("D MMM YYYY HH:mm:ss").toUpperCase()
        this.dbg.log("setting time to", s)
        const cmd = `hwclock --set --date "${s}" && hwclock -s`
        return this.exec('setTime', cmd)
    }

    shutdown(restart: boolean) {
        this.dbg.log("executing shutdown, restart:", restart)
        const cmd = restart ? 'shutdown -r now' : 'shutdown -h now'
        return this.exec('shutdown', cmd)
    }

    getBuildString(buildDateFilePath = './build.date') {
        return new Promise<string>(resolve => {
            fs.readFile(buildDateFilePath, 'utf8', (err, data) => {
                if (err) { this.dbg.error('getBuildString', err) }
                const buildString = data && data.trim() || undefined
                resolve(buildString)
            })
        })
    }
    
    private exec(logId: string, cmd: string) {
        return new Promise<boolean>(resolve => {
            exec(cmd,
                (err, stdout, stderr) => {
                    if (err) {
                        this.dbg.error(logId + ' (err)', err)
                        resolve(false)
                    } else if (stderr) {
                        this.dbg.error(logId + ' (stderr)', stderr)
                        resolve(false)
                    } else {
                        this.dbg.log(logId, stdout)
                        resolve(true)
                    }
                }
            )
        })
    }
}