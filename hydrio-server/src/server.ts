import { ServerBase } from 'lib/server-base'
import { HydrioDBSqlite } from 'hydrio-db-sqlite'
import { HydrioWebAPI } from 'hydrio-web-api'
import { HydrioController } from '@shared'
import { HydrioPiPlatformController } from 'hydrio-pi-platform-controller'
import { HydrioPiHardwareController } from 'hydrio-pi-hardware-controller'

export class HydrioServer extends ServerBase {

  private hc: HydrioController

  protected start() {
    this.hc = new HydrioController(
      new HydrioPiPlatformController(),
      new HydrioPiHardwareController(),
      new HydrioDBSqlite()
    )
    return this.hc.start()
      .then(() => new HydrioWebAPI(this.hc))
  }

  dispose() {
    this.hc.dispose()
  }
}

export default new HydrioServer()
