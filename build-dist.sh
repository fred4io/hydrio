#!/bin/bash

set -e

#build client files
cd hydrio-client
npm run build
cd ..

#build server files
cd hydrio-server
rm -rf build
npm run build
cd ..

#output dir
rm -rf ./dist ||:
mkdir dist
#server files
mv hydrio-server/build/hydrio-server/src dist/server
#shared files
mv hydrio-server/build/hydrio-shared dist/server/shared
#clean
rm -rf hydrio-server/build
#config files
cp hydrio-server/package.json dist/server
#client files
mkdir dist/server/public
cp -r hydrio-client/dist/* dist/server/public
#other files
cp -r dist-src/* dist
#scripts
for file in `ls -1 dist/*.sh`; do
    chmod +x $file
done
#build date
date +"%Y-%m-%dT%H:%M:%S %z" > dist/server/build.date