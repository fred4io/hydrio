import { Injectable, Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from "@angular/material";

@Injectable()
export class ConfirmDialogService {

    constructor(private dialog: MatDialog) {
    }

    deletion(elementText?: string, elementId?: string, moreText?: string) {
        if (elementId) { elementId = "'" + elementId + "'" }
        const text = "Supprimer "
            + (elementText || '')
            + (elementId ? (' ' + elementId) : '')
            + (moreText ? (' ' + moreText) : '')
            + " ?"
        return this.show(text, undefined, "Oui, supprimer", "Non, conserver")
    }

    show(text: string, title?: string, confirmButtonText?: string, cancelButtonText?: string) {
        return new Promise<boolean>(resolve => {
            const sub = this.dialog.open(ConfirmDialogComponent,
                {
                    data: {
                        text: text,
                        title: title,
                        confirmButtonText: confirmButtonText,
                        cancelButtonText: cancelButtonText,
                    },
                    panelClass: 'confirmOverlay'
                }
            ).afterClosed().subscribe(confirmed => {
                sub.unsubscribe()
                resolve(confirmed)
            })
        })
    }
}

@Component({
    template: `
      <h1 mat-dialog-title *ngIf="title">{{ title }}</h1>
      <mat-dialog-content>{{ text }}</mat-dialog-content>
      <mat-dialog-actions>
        <button mat-button (click)="cancel()">{{ cancelButtonText }}</button>
        <button mat-button (click)="confirm()" color="warn">{{ confirmButtonText }}</button>
      </mat-dialog-actions>
    `})
export class ConfirmDialogComponent implements OnInit {

    public title: string
    public text: string
    public confirmButtonText: string
    public cancelButtonText: string

    constructor(
        public dialogRef: MatDialogRef<ConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any) {
    }

    ngOnInit() {
        this.title = this.data.title
        this.text = this.data.text
        this.confirmButtonText = this.data.confirmButtonText || "Confirmer"
        this.cancelButtonText = this.data.cancelButtonText || "Annuler"
    }

    confirm() {
        this.dialogRef.close(true)
    }

    cancel() {
        this.dialogRef.close(false)
    }
}