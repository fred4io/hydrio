import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { environment } from 'environments/environment'
import * as moment from 'moment'
import { ApiClient } from '../ApiClient'
import { ErrorService } from './error-service.service'
import {
    HParams, HDevice, HSequence, HItem, HSequenceGenerateInfo, HStatus,
    HydrioController, HydrioHardwareControllerMock, HydrioDbMock, HSystemInfo,
    HydrioPlatformControllerMock
} from 'hydrio-shared'

export class HydrioServiceError {
    constructor(
        public err: any,
        public funcName: string
    ) { }
}

@Injectable()
export class HydrioService {

    public sysInfo: HSystemInfo

    private traceConnection = false

    private mockAPI = environment.mockAPI || HParams.mockAPI
    private mock: HydrioController

    private api: ApiClient
    private isReady: boolean

    private devices: HDevice[]
    private singleActive = true

    private masterDeviceId: number

    private sequences: HSequence[]
    private activeSequence: HSequence

    private isLocked: boolean


    get ready() { return this.isReady }
    get usingMaster() { return HParams.useMasterDevice }

    constructor(http: HttpClient, private errorService: ErrorService) {
        console.log('service init')

        if (this.mockAPI) {
            console.log('mocking API')
            localStorage.debug = '*,-"sockjs-client":*'
            this.mock = new HydrioController(
                new HydrioPlatformControllerMock(),
                new HydrioHardwareControllerMock(),
                new HydrioDbMock(true)
            )
            this.mock.start().then(() => this.init(http))
        } else {
            delete localStorage.debug
            this.init(http)
        }
    }

    private init(http: HttpClient) {

        this.api = new ApiClient(http, '/api/',
            (err, logId) => this.handleError(err, 'ApiClient ' + logId),
            true,
            this.mockAPI
        )

        this.devices = new Array<HDevice>()

        this.loopInit()
    }

    private loopInit() {
        this.initInfo()
            .then(() =>
                this.sysInfo.i2cEnabled &&
                this.initDevices()
                    .then(() => this.initSequences())
            ).then(() => {
                console.log('service ready')
                this.isReady = true
            }).catch(err => {
                this.handleError(err, 'init')
                setTimeout(() => this.loopInit(), 1000)
            })

    }

    isMasterDevice(deviceId: number) {
        this.checkReady()
        return deviceId == this.masterDeviceId
    }
    getMasterDevice() {
        this.checkReady()
        const result = this.devices.find(d => d.id == this.masterDeviceId)
        // console.log('getMasterDevice', result, this.masterDeviceId, this.devices)
        return result
    }
    setMasterDevice(device: HDevice) {
        this.checkReady()
        const id = device && device.id
        return this.api.putData('masterdeviceid', { id }
            /* mock */, () => this.mock.setMasterDeviceId(id)
        ).then(() => {
            this.masterDeviceId = id
            console.log('setMasterDevice: ' + id)
        })
    }
    getSingleActive() {
        this.checkReady()
        return this.singleActive
    }
    setSingleActive(singleActive: boolean) {
        this.checkReady()
        return this.api.putData('singleactive', { singleActive: singleActive }
            /* mock */, () => this.mock.setSingleActive(singleActive)
        ).then(() => this.singleActive = singleActive)
    }

    getDevices() {
        this.checkReady()
        this.devices.forEach(d => {
            d.isReserved = this.isDeviceReserved(d)
            if (d.isDead || d.isReserved) { d.state = undefined }
        })
        return this.devices
    }

    getAllDevicesStates() {
        this.checkReady()
        return this.api.getData('devicestates'
            /* mock */, () => this.mock.getAllStates()
        )
    }

    getDeviceState(device: HDevice) {
        this.checkReady()
        return this.api.getItem('devicestate', device && device.id
            /* mock */, () => ({ state: this.mock.getState(device && device.id) })
        ).then(data => data && data.state)
    }
    setDeviceState(device: HDevice, activeState: boolean) {
        this.checkReady()
        return this.api.putItem<Object, void>('devicestate', device && device.id,
            { state: activeState }
            /* mock: */, () => this.mock.setState(device.id, activeState)
        )
    }

    setDeviceDead(device: HDevice) {
        this.checkReady()
        return this.api.putItem<Object, void>('devicedead', device && device.id,
            { isDead: device.isDead }
            /* mock */, () => this.mock.setDeviceDead(device.id, device.isDead)
        )
    }

    getSequences() {
        this.checkReady()
        return new Promise<HSequence[]>(resolve => resolve(this.sequences))
    }

    isActiveSequence(seq: HSequence) {
        this.checkReady()
        return this.activeSequence == seq
    }
    getActiveSequence() {
        this.checkReady()
        return this.activeSequence
    }
    setActiveSequence(seq: HSequence) {
        this.checkReady()
        const seqId = seq && seq.id
        return this.api.putData('activesequenceid', { id: seqId }
            /* mock */, () => this.mock.setActiveSequenceId(seqId)
        ).then(() => this.activeSequence = seq)
    }
    getStatus() {
        this.checkReady()
        return this.api.getData('status'
            /* mock */, () => this.mock.getStatus()
        ).then(data => HStatus.fromData(data))
    }

    isManualDisabled() {
        this.checkReady()
        return !!this.activeSequence && this.isLocked
    }
    isAuto() {
        this.checkReady()
        return !!this.activeSequence;
    }
    getLocked() {
        this.checkReady()
        return this.isLocked
    }
    setLocked(locked: boolean) {
        this.checkReady()
        return this.api.putData('islocked', { locked: locked }
            /* mock */, () => this.mock.setIsLocked(locked)
        ).then(() => this.isLocked = locked)
    }

    getSequenceInfo(seqId: number) {
        return this.sequences.find(s => s.id == seqId)
    }

    getSequence(seqId: number = 0) {
        this.checkReady()
        console.log('getSequence', seqId)
        const sequence = this.getSequenceInfo(seqId)
        return this.getSequenceItems(sequence)
    }

    setSequenceInfo(seq: HSequence) {
        this.checkReady()
        const seqInfo = seq.clone()
        seqInfo.items = undefined
        return this.api.putItem<HSequence, void>('sequence', seqInfo.id, seqInfo
            /* mock */, () => this.mock.updateSequence(seq)
        ).then(() => {
            console.log('setSequenceInfo', seq.name, seq.startTime, seq.repeat)
        })
    }
    addSequence(info = new HSequenceGenerateInfo()) {
        this.checkReady()
        return this.api.postData<HSequenceGenerateInfo, HSequence>('sequence', info
            /* mock */, () => this.mock.createSequence(info)
        ).then(HSequence.fromData
        ).then(seq => {
            console.log('addSequence', seq.id)
            this.sequences.push(seq)
            return seq
        })
    }
    deleteSequence(seq: HSequence) {
        this.checkReady()
        const seqId = seq.id
        const seqIndex = this.sequences.findIndex(s => s.id == seqId)
        if (seq == this.activeSequence) {
            return this.errorPromise<void>("séquence active")
        }
        if (seqIndex == -1) {
            return this.errorPromise<void>("séquence inexistante")
        }
        return this.api.deleteItem<void>('sequence', seqId
            /* mock */, () => this.mock.deleteSequence(seqId)
        ).then(() => {
            console.log('deleteSequence', seqId)
            this.sequences.splice(seqIndex, 1)
            // if (this.sequences.length == 1) {
            //     return this.setActiveSequence(this.sequences[0])
            // }
        })
    }

    deleteSequenceItem(item: HItem) {
        this.checkReady()
        return this.api.deleteItem<void>('sequenceitem', item.id
            /* mock */, () => this.mock.deleteSequenceItem(item && item.id)
        )
    }
    addSequenceItem(item: HItem) {
        this.checkReady()
        return this.api.postData<HItem, HItem>('sequenceitem', item
            /* mock */, () => this.mock.createSequenceItem(item)
        ).then(data => HItem.fromData(data))
    }
    updateSequenceItem(item: HItem) {
        this.checkReady()
        return this.api.putItem<HItem, void>('sequenceitem', item.id, item
            /* mock */, () => this.mock.updateSequenceItem(item)
        )
    }

    canEdit(seq: HSequence) {
        return this.isReady && seq != this.activeSequence
    }

    canDelete(seq: HSequence) {
        return this.isReady && this.sequences.length > 1 && this.canEdit(seq)
    }

    getMaxNbDevices() {
        this.checkReady()
        const nbNotAvailable = this.devices.reduce((p, d) =>
            (d.isDead || this.isDeviceReserved(d)) ? (p + 1) : p, 0)
        return HParams.nbDevices - nbNotAvailable
    }

    shutdown() {
        this.api.postData('shutdown', { restart: false },
            /* mock */() => this.mock.shutdown(false)
        )
    }
    simulatePowerLoss() {
        this.api.postData('simulatepowerloss', { restart: false },
            /* mock */() => this.mock.simulatePowerLoss()
        )
    }
    setSystemTime() {
        this.api.postData('setsystemtime', { now: moment() },
            /* mock */() => { }
        )
    }


    tryTillOnline(callback: () => void | Promise<any>, cancel: () => boolean) {
        this.whenReady(() => {
            if (cancel && cancel()) {
                if (this.traceConnection) { console.log('canceled before testing connection') }
            } else {
                this.loopTestConnection(callback, cancel)
            }
        })
    }


    private loopTestConnection(callback: () => void | Promise<any>, cancel: () => boolean, retries = 0) {
        ++retries
        if (this.traceConnection) { console.log('testing connection, attempt #' + retries) }
        this.api.getData('testconnection')
            .then(() => {
                if (this.traceConnection) { console.log('loopTestConnection: server reached') }
                if (cancel && cancel()) {
                    if (this.traceConnection) { console.log('loopTestConnection: canceled before execute callback') }
                } else {
                    if (this.traceConnection) { console.log('loopTestConnection: executing callback') }
                    callback()
                }
            }).catch(err => {
                if (cancel && cancel()) {
                    if (this.traceConnection) { console.log('loopTestConnection: error, canceled') }
                    return
                } else if (err.status == 504) {
                    if (this.traceConnection) { console.log('loopTestConnection: timeout error => retrying') }
                    setTimeout(() => this.loopTestConnection(callback, cancel, retries), 500)
                } else {
                    if (this.traceConnection) { console.log('loopTestConnection: other error') }
                    throw err
                }
            })
    }
    private whenReady(callback: (() => void) | Promise<any>) {
        if (callback instanceof Promise) {
            return new Promise<any>(resolve =>
                this.onReady(resolve)
            ).then(() => {
                if (this.traceConnection) { console.log('ready, executing resolving promise') }
                return callback
            })
        } else {
            return new Promise<any>(resolve => {
                this.onReady(() => {
                    if (this.traceConnection) { console.log('ready, executing callback') }
                    callback()
                    resolve()
                })
            })
        }
    }
    private onReady(callback: () => void) {
        if (this.isReady) {
            if (this.traceConnection) { console.log('ready') }
            callback()
        } else {
            if (this.traceConnection) { console.log('not ready, waiting') }
            setTimeout(() => this.onReady(callback), 500);
        }
    }


    private errorPromise<T>(msg: string) {
        return new Promise<T>((_, reject) => reject(msg))
    }

    private handleError(err: any, context: string) {
        console.error('handleError', context, err)
        this.errorService.signal(err, 'hydrio-service/' + context)
    }

    private checkReady() {
        if (!this.isReady) { throw new Error('service not ready') }
    }

    private isDeviceReserved(device: HDevice) {
        return device && device.id == this.masterDeviceId
    }

    private initInfo() {
        return this.api.getData('systeminfo'
            /* mock */, () => this.mock.getSystemInfo()
        ).then(info => this.sysInfo = info)
    }

    private initDevices() {
        console.log('initDevices begin')
        return this.api.getData('singleactive'
            /* mock */, () => this.mock.getSingleActive()
        ).then(singleActive => {
            this.singleActive = singleActive
            return HParams.useMasterDevice
        }).then(useMaster => {
            // console.log('useMaster', useMaster)
            if (useMaster) {
                return this.api.getData<number>('masterdeviceid'
                    /* mock */, () => this.mock.getMasterDeviceId()
                )
            } else {
                return this.api.putData('masterdeviceid', null
                    /* mock */, () => this.mock.setMasterDeviceId(null)
                ).then(() => undefined)
            }
        }).then(masterDeviceId => {
            // console.log('masterDeviceId', masterDeviceId)
            this.masterDeviceId = masterDeviceId
            return this.api.getData<HDevice[]>('device'
                    /* mock */, () => this.mock.getDevices()
            )
        }).then(data => {
            this.devices = data.map(HDevice.fromData)
            console.log('initDevices end')
        })
    }

    private initSequences() {
        console.log('initSequences begin')
        return this.api.getData<HSequence[]>('sequence'
            /* mock */, () => this.mock.getSequences()
        ).then(data => {
            this.sequences = data.map(s => HSequence.fromData(s))
            // TODO: wait all
            const ps = this.sequences.map(s => this.getSequenceItems(s))
            return this.api.getData<number>('activesequenceid'
                /* mock */, () => this.mock.getActiveSequenceId()
            )
        }).then(id => {
            this.activeSequence = this.sequences.find(s => s.id == id)
            return this.api.getData<boolean>('islocked'
                /* mock */, () => this.mock.getIsLocked()
            )
        }).then(locked => {
            this.isLocked = !!locked
            console.log('initSequences end')
        })
    }

    private getSequenceItems(sequence: HSequence) {
        const seqId = sequence && sequence.id
        return this.api.getItem<HItem[]>('sequenceitems', seqId
            /* mock */, () => this.mock.getSequenceItems(sequence && sequence.id)
        ).then(data => {
            // console.log(data)
            if (sequence) {
                sequence.items = data
                    .map(d => HItem.fromData(d, this.devices))
                    .sort((a, b) => a.position - b.position)
            }
            return sequence
        })
    }
}