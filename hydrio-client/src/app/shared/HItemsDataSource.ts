import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject } from 'rxjs';
import { HItem, HSequence, HParams, HUtils } from 'hydrio-shared';

export class HItemsDataSource extends DataSource<HItem> {

    private itemsSubject = new BehaviorSubject<HItem[]>([]);

    constructor(seq: HSequence) {
        super()

        const items = seq.items, nbCycles = HParams.seqMaxCycles
        function getStartTime(item: HItem, cycleIndex: number) {
            const index = items.indexOf(item)
            if (index == -1) { throw new Error("unknown item") }
            if (index == 0 && cycleIndex == 0) { return seq.startTime + ':00' }
            const pidx = (index - 1 + items.length) % items.length
            const prev = items[pidx]
            const pci = pidx >= index ? (cycleIndex - 1) : cycleIndex
            return HUtils.addSecondsToHMS((prev as any).startTimes[pci], prev.durationSeconds)
        }
        items.forEach(it => (it as any).startTimes = new Array<string>())
        for (let ci = 0; ci < nbCycles; ci++) {
            items.forEach(it => (it as any).startTimes[ci] = getStartTime(it, ci))
        }

        this.itemsSubject.next(items)
    }

    connect() {
        return this.itemsSubject.asObservable()
    }

    disconnect() {
        this.itemsSubject.complete()
    }

}