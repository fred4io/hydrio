import { Component, Input } from '@angular/core'

@Component({
    selector: 'app-yes-no',
    template:
        '<span class="yes-no">'
        + '<span *ngIf="value" class="yes-no-yes">{{yes}}</span>'
        + '<span *ngIf="!value" class="yes-no-no">{{no}}</span>'
        + '<span>'
})
export class YesNoComponent {

    @Input() value = false
    @Input() yes = "oui"
    @Input() no = "non"

}
