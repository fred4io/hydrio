import { Directive } from "@angular/core"
import { Validator, NG_VALIDATORS, ValidationErrors, AbstractControl } from "@angular/forms"
import { HDevice } from "hydrio-shared";

@Directive({
    selector: '[appValidSelectedDevice]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: ValidSelectedDeviceDirective,
        multi: true
    }]
})
export class ValidSelectedDeviceDirective implements Validator {

    validate(control: AbstractControl): ValidationErrors | null {
        const device = control.value as HDevice
        return !device ? null
            : device.isReserved ? { reserved: true }
                : device.isDead ? { dead: true }
                    : null
    }

}