import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs"

export class ApiClient {

    constructor(
        private http: HttpClient,
        private urlPrefix = '/api/',
        private onError?: (err: any, logId?: string) => void,
        public logSuccess = true,
        public mock = false
    ) {
    }

    public getData<TRes>(apiUrl: string
        , mock?: () => TRes
    ) {
        return new Promise<TRes>((resolve, reject) => {
            const logId = 'getData ' + apiUrl
            const obs =
                this.mock ? this.mockedObs(logId, mock)
                    : this.http.get(this.urlPrefix + apiUrl)
            this.processObs(logId, obs, resolve, reject)
        })
    }
    public putData<TReq, TRes>(apiUrl: string, body: TReq
        , mock?: () => TRes
    ) {
        if (body == undefined) {
            return this.errorPromise<TRes>("no data body", apiUrl)
        }
        return new Promise<TRes>((resolve, reject) => {
            const logId = 'putData ' + apiUrl
            const obs = this.mock ? this.mockedObs(logId, mock)
                : this.http.put(this.urlPrefix + apiUrl, body)
            this.processObs(logId, obs, resolve, reject)
        })
    }
    public postData<TReq, TRes>(apiUrl: string, body?: TReq
        , mock?: () => TRes
    ) {
        return new Promise<TRes>((resolve, reject) => {
            const logId = 'postData ' + apiUrl
            const obs = this.mock ? this.mockedObs(logId, mock)
                : this.http.post(this.urlPrefix + apiUrl, body)
            this.processObs(logId, obs, resolve, reject)
        })
    }
    public deleteData<TRes>(apiUrl: string
        , mock?: () => TRes
    ) {
        return new Promise<TRes>((resolve, reject) => {
            const logId = 'deleteData ' + apiUrl
            const obs = this.mock ? this.mockedObs(logId, mock)
                : this.http.delete(this.urlPrefix + apiUrl)
            this.processObs(logId, obs, resolve, reject)
        })
    }
    public getItem<TRes>(apiUrl: string, itemId: number
        , mock?: () => TRes
    ) {
        if (itemId == undefined) {
            return this.errorPromise<TRes>("no item id", apiUrl)
        }
        return this.getData<TRes>(apiUrl + '/' + itemId, mock)
    }
    public putItem<TReq, TRes>(apiUrl: string, itemId: number, body: TReq
        , mock?: () => TRes
    ) {
        if (itemId == undefined) {
            return this.errorPromise<TRes>("no item id", apiUrl)
        }
        if (body == undefined) {
            return this.errorPromise<TRes>("no item body", apiUrl)
        }
        return this.putData<TReq, TRes>(apiUrl + '/' + itemId, body, mock)
    }
    public deleteItem<TRes>(apiUrl: string, itemId: number
        , mock?: () => TRes
    ) {
        if (itemId == undefined) {
            return this.errorPromise<TRes>("no item id", apiUrl)
        }
        return this.deleteData<TRes>(apiUrl + '/' + itemId, mock)
    }

    private processObs<T>(logId: string, obs: Observable<Object>,
        resolve: (data: T) => void, reject: (err: any) => void) {
        obs.subscribe((data: T) => {
            if (this.logSuccess) { console.log(logId, 'done') }
            resolve(data)
        }, err => {
            this.handleError(err, logId)
            reject(err)
        })
    }

    private handleError(err: any, logId) {
        if (this.onError) {
            this.onError(err, logId)
            return true
        } else {
            console.error('handleError', logId, err)
            return false
        }
    }

    private errorPromise<TRes>(msg: string, apiUrl: string) {
        this.handleError(msg, apiUrl)
        return new Promise<TRes>((_, reject) => reject(apiUrl + ": " + msg))
    }

    private mockedObs<TRes>(logId: string, mock?: () => TRes) {
        console.log('MOCKING ' + logId)
        return new Observable(o => { if (mock) { o.next(mock()) } else { o.next() } })
    }
}