import { OnInit, OnDestroy } from '@angular/core'
import { HydrioService } from '.'

export class ConnectedComponentBase implements OnInit, OnDestroy {

    public connecting: boolean
    public isReady: boolean
    protected destroying: boolean

    constructor(
        protected hydrioService: HydrioService
    ) {
    }

    ngOnInit() {
        // display the 'connecting...' message only after 1s
        const ts = setTimeout(() => this.connecting = true, 1000)

        const done = () => {
            // hide the connecting message
            clearTimeout(ts)
            this.connecting = false
            // display the content
            this.isReady = true
        }

        // wait for the service to be ready,
        // and retry the init function while network error
        // or cancels when leaving the page
        return this.hydrioService.tryTillOnline(() => {
            const p = this.init(done)
            if (p instanceof Promise) {
                p.then(done) // .catch(err => { throw err })
            }
        }, () => this.destroying)
    }

    ngOnDestroy() {
        this.destroying = true
        this.destroy()
    }

    protected init(done: () => void): Promise<any> | void {
        done()
    }

    protected destroy() {
    }

}
