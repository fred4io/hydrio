import { Pipe, PipeTransform } from '@angular/core';
import { HUtils } from 'hydrio-shared';

@Pipe({ name: 'secondsToHMM' })
export class SecondsToHMMPipe implements PipeTransform {
    transform(value: number): string {
        return HUtils.secondsToHMM(value)
    }
}

