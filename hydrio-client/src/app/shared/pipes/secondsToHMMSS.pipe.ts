import { Pipe, PipeTransform } from '@angular/core';
import { HUtils } from 'hydrio-shared';

@Pipe({ name: 'secondsToHMMSS' })
export class SecondsToHMMSSPipe implements PipeTransform {
  transform(value: number): string {
    return HUtils.secondsToHMMSS(value)
  }
}