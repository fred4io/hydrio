import { Pipe, PipeTransform } from '@angular/core';
import { HUtils } from 'hydrio-shared';

@Pipe({ name: 'secondsToMMSS' })
export class SecondsToMMSSPipe implements PipeTransform {
  transform(value: number): string {
    return HUtils.secondsToMMSS(value)
  }
}