import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject } from 'rxjs';
import { HDevice } from 'hydrio-shared';

export class HDevicesDataSource extends DataSource<HDevice> {

    private devicesSubject = new BehaviorSubject<HDevice[]>([])

    constructor(public devices: HDevice[]) {
        super()
        this.devicesSubject.next(devices);
    }

    connect() {
        return this.devicesSubject.asObservable()
    }

    disconnect() {
        this.devicesSubject.complete();
    }

}