import { Component, HostBinding } from '@angular/core'
import { HydrioService } from './shared'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  @HostBinding('class') class = 'app'

  title = 'Hydrio'

  public get isAuto() { return this.hydrioService.ready && this.hydrioService.isAuto() }

  public get manualDisabled() { return !this.hydrioService.ready || this.hydrioService.isManualDisabled() }

  constructor(
    private hydrioService: HydrioService
  ) {
  }

}
