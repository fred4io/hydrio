import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core'
import { HydrioService, HDevicesDataSource, ConnectedComponentBase } from '../shared'
import { HDevice } from 'hydrio-shared'

@Component({
    selector: 'app-devices',
    styleUrls: ['./devices.component.scss'],
    templateUrl: './devices.component.html'
})
export class DevicesComponent extends ConnectedComponentBase implements AfterViewInit {

    devices: HDevice[]
    datasource: HDevicesDataSource
    master: HDevice
    columnsToDisplay = ['name', 'dead', 'onOff']
    usingMaster = true

    get singleActive() { return this._singleActive }
    set singleActive(value: boolean) {
        this.hydrioService.setSingleActive(value)
            .then(() => {
                this._singleActive = value
                this.getDevicesStates()
            })
    }
    private _singleActive = true

    constructor(
        hydrioService: HydrioService,
        private cdr: ChangeDetectorRef
    ) {
        super(hydrioService)
    }

    ngAfterViewInit() {
    }

    protected init() {
        return this.hydrioService.setActiveSequence(undefined)
            .then(() => {
                this._singleActive = this.hydrioService.getSingleActive()
                this.devices = this.hydrioService.getDevices()
                this.usingMaster = this.hydrioService.usingMaster
                this.master = this.usingMaster ? this.hydrioService.getMasterDevice() : undefined
                this.getDevicesStates()
            }).then(() => {
                this.updateDataSource()
            })
    }

    masterChanged(device: HDevice) {
        if (!this.usingMaster) { return }
        this.hydrioService.setMasterDevice(device)
            .then(() => {
                this.master = device
                return this.getDevicesStates()
            }).then(() => {
                this.updateDataSource()
                this.cdr.detectChanges()
            }).catch(err => console.error('masterChanged', err))
    }

    toggleState(device: HDevice) {
        console.log('toggleState')
        const newState = !device.state
        this.hydrioService.setDeviceState(device, newState)
            .then(() => this.getDevicesStates())
            // .then(() => this.cdr.detectChanges())
            .catch(err => console.error('setDeviceState', err))
    }

    setDeviceDead(device: HDevice) {
        this.hydrioService.setDeviceDead(device)
            .then(() => device.state = device.isDead ? undefined : false)
            .catch(err => console.error('setDeviceDead', err))
    }


    private updateDataSource() {
        this.datasource = new HDevicesDataSource(
            this.devices.filter(d => d != this.master))
    }

    private getDevicesStates() {
        return this.hydrioService.getAllDevicesStates()
            .then(dss => {
                this.devices.forEach(d => {
                    const ds = dss.find(o => o.id == d.id)
                    d.state = ds && ds.state
                })
            })
    }
}
