import { Component } from '@angular/core'
import { MatDialogRef } from '@angular/material'
import { HydrioService, ConnectedComponentBase } from '../../shared'
import { HSequenceGenerateInfo } from 'hydrio-shared'

@Component({
  selector: 'app-sequence-generator',
  templateUrl: './sequence-generator.component.html',
  styleUrls: ['./sequence-generator.component.scss']
})
export class SequenceGeneratorComponent extends ConnectedComponentBase {

  public info = new HSequenceGenerateInfo()

  public maxNbElements = 1

  constructor(
    public dialogRef: MatDialogRef<SequenceGeneratorComponent, HSequenceGenerateInfo>,
    hydrioService: HydrioService
  ) {
    super(hydrioService)
  }

  protected init(done: () => void) {
    this.maxNbElements = this.hydrioService.getMaxNbDevices()
    done()
  }

  public submit() {
    console.log('submit')
    this.dialogRef.close(this.info)
  }

}
