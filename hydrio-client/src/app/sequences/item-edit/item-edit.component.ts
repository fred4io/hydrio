import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmDialogService, ShowOnInvalidStateMatcher } from '../../shared';
import { HParams, HItem, HDevice, HUtils } from 'hydrio-shared';

@Component({
    selector: 'app-item-edit',
    styleUrls: ['./item-edit.component.scss'],
    templateUrl: './item-edit.component.html'
})
export class ItemEditComponent {

    public item: HItem
    public isNew: boolean
    public isSingle: boolean
    public itemIndex: number
    public isActive: boolean

    public devices: HDevice[]
    public indices: number[]

    public matcher = new ShowOnInvalidStateMatcher()

    public get canDelete() { return !this.isNew && !this.isSingle && !this.isActive }

    constructor(
        public dialogRef: MatDialogRef<ItemEditComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private confirm: ConfirmDialogService
    ) {
        this.item = data.item.clone()
        this.isNew = data.isNew
        this.isSingle = data.isSingle
        this.isActive = data.isActive
        this.devices = data.devices
        this.itemIndex = data.itemIndex
        this.indices = Array.from(Array(data.nbItems), (x, i) => i)
        if (!this.devices.length) { this.deviceSelected(this.item.device) }
    }

    public submit() {
        this.data.item.copy(this.item)
        this.dialogRef.close({ itemIndex: this.itemIndex })
    }
    public delete() {
        this.confirm.deletion("l'élément", this.item.name,
            "en position " + (1 + this.item.position)
        )
            .then(confirmed => confirmed &&
                this.dialogRef.close({
                    itemIndex: this.itemIndex,
                    delete: true
                })
            )
    }

    public deviceSelected(d: HDevice) {
        // console.log('deviceSelected', this.item)
        if (d == undefined) {
            this.item.name = HParams.itemPauseName
        } else if (this.item.name == HParams.itemPauseName) {
            this.item.name = HUtils.getNewSequenceItemName(this.data.seq)
        }
    }

}
