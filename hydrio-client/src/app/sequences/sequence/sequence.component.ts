import { Component, ChangeDetectorRef } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { MatDialog } from '@angular/material'
import { ItemEditComponent } from '../item-edit/item-edit.component'
import { HydrioService, HItemsDataSource, ConnectedComponentBase } from '../../shared'
import { HDevice, HSequence, HUtils, HParams, HItem } from 'hydrio-shared'

@Component({
    selector: 'app-sequence',
    styleUrls: ['./sequence.component.scss'],
    templateUrl: './sequence.component.html'
})
export class SequenceComponent extends ConnectedComponentBase {

    public seqStartTimeStepMinutes = HParams.seqStartTimeStepMinutes
    public seqStartTimeMaxMinutes = 24 * 60 - HParams.seqStartTimeStepMinutes
    public seqStartTimeTickInterval = 60 / HParams.seqStartTimeStepMinutes

    public seq: HSequence
    public devices: HDevice[]
    public readonly cyclesIndices = Array.from(new Array(HParams.seqMaxCycles), (_, i) => i)

    public hhmmToMinutes = HUtils.hhmmToMinutes
    public minutesToHHMM = HUtils.minutesToHHMM

    private saveTimeout: any
    private sub: any

    constructor(
        private route: ActivatedRoute,
        private dialog: MatDialog,
        private cdr: ChangeDetectorRef,
        hydrioService: HydrioService
    ) {
        super(hydrioService)
    }

    protected init(done: () => void) {
        this.devices = this.hydrioService.getDevices()
        this.sub = this.route.params.subscribe(params => {
            const seqId = +params['id']
            this.initWhenSeqId(seqId)
        })
        done()
    }

    private initWhenSeqId(seqId: number) {
        this.hydrioService.getSequence(seqId)
            .then(seq => {
                // console.log('getSequence', seq)
                this.seq = seq
            }).catch(err => { throw err })
    }

    destroy() {
        this.sub.unsubscribe()
    }

    public get items() { return this.seq && this.seq.items }

    public get isActive() { return this.hydrioService.isActiveSequence(this.seq) }

    public get columnsToDisplay() {
        const columns = ['num', 'nom']
        if (HParams.seqShowDevice) { columns.push('device') }
        columns.push('duree')
        const cycles = this.seq && this.seq.repeat || 1
        for (let i = 0; i < cycles; i++) { columns.push('cycle' + (i + 1)) }
        return columns
    }

    public get datasource() { return new HItemsDataSource(this.seq) }


    public cycleTime(it: HItem, cycleIndex: number) {
        if (this.isPause(it)) { return '' }
        const hms: string = (it as any).startTimes[cycleIndex]
        return HParams.seqShowCyclesSeconds ? hms : hms.substr(0, hms.length - 3)
    }
    public setStartTimeSeconds(seconds: number) {
        this.seq.startTime = this.minutesToHHMM(seconds)
        this.saveSequenceInfo()
    }
    public setRepeat(repeat: number) {
        this.seq.repeat = repeat
        this.saveSequenceInfo()
    }
    public setName(name: string) {
        this.seq.name = name
        if (name) { this.saveSequenceInfo() }
    }

    public addItemClicked() {
        const name = HUtils.getNewSequenceItemName(this.seq)
        const device = this.getAvailDevices().find(d => true)
        this.edit(new HItem(this.seq.id, -1, name, device), true);
    }

    public editItemClicked(item: HItem) {
        this.edit(item);
    }

    private getAvailDevices(includeThis?: HDevice) {
        const usedDevices = this.items.map(it => it.device)
        const result = this.devices.filter(d =>
            d && (
                includeThis && d == includeThis
                ||
                !d.isReserved && !d.isDead && !usedDevices.some(ud => ud == d)
            )
        )
        // console.log('getAvailDevices', this.devices, usedDevices, result)
        return result
    }

    private edit(item: HItem, isNew = false) {
        // console.log('edit', item)
        const itemIndex = isNew ? this.items.length : this.items.indexOf(item),
            isSingle = this.items.length == 1
        this.dialog.open(ItemEditComponent,
            {
                data: {
                    item,
                    isNew,
                    isSingle,
                    itemIndex,
                    devices: this.getAvailDevices(item.device),
                    nbItems: this.items.length + (isNew ? 1 : 0),
                    seq: this.seq,
                    isActive: this.isActive
                },
                panelClass: 'itemEditOverlay',
                autoFocus: false
            }
        ).afterClosed().subscribe(result => {
            if (!result || this.isActive) { return }
            item.position = result.itemIndex
            const action =
                result.delete ?
                    this.hydrioService.deleteSequenceItem(item)
                    : isNew ?
                        this.hydrioService.addSequenceItem(item)
                            .then(data => { item.copy(data) })
                        : this.hydrioService.updateSequenceItem(item)
            action.then(() => {
                // console.log('item id:', item.id)
                if (!isNew) { this.items.splice(itemIndex, 1) }
                if (!result.delete) { this.items.splice(result.itemIndex, 0, item) }
                this.cdr.detectChanges()
            })
        })
    }

    private isPause(it: HItem) {
        const paused = it && (!it.device || !it.device.address || it.device.isReserved || it.device.isDead)
        // console.log(it, paused)
        return paused
    }

    private saveSequenceInfo() {
        clearTimeout(this.saveTimeout)
        this.saveTimeout = setTimeout(() => this.hydrioService.setSequenceInfo(this.seq), 200)
    }
}
