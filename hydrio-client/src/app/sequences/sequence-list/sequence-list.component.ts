import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { MatDialog } from '@angular/material'
import { HydrioService, ConfirmDialogService, ConnectedComponentBase } from '../../shared'
import { SequenceGeneratorComponent } from '../sequence-generator/sequence-generator.component'
import { HSequence } from 'hydrio-shared'

@Component({
    selector: 'app-sequences',
    styleUrls: ['./sequence-list.component.scss'],
    templateUrl: './sequence-list.component.html'
})
export class SequenceListComponent extends ConnectedComponentBase {

    public sequences: HSequence[]

    constructor(
        private router: Router,
        private dialog: MatDialog,
        private confirm: ConfirmDialogService,
        hydrioService: HydrioService,
    ) {
        super(hydrioService)
    }

    protected init() {
        return this.hydrioService.getSequences()
            .then(result => this.sequences = result)
    }

    public editClicked(seq: HSequence) {
        this.router.navigate(['/sequence', seq.id])
    }
    public addClicked() {
        this.dialog.open(SequenceGeneratorComponent, {
            autoFocus: false
        }).afterClosed().subscribe(info => {
            if (info) {
                this.hydrioService.addSequence(info).then(seq => {
                    this.editClicked(seq)
                })
            }
        })
    }

    public deleteClicked(seq: HSequence) {
        this.confirm.deletion("la séquence", seq.name)
            .then(confirmed => confirmed &&
                this.hydrioService.deleteSequence(seq)
            )
    }

    public canEdit(seq: HSequence) {
        return this.hydrioService.canEdit(seq)
    }

    public canDelete(seq: HSequence) {
        return this.hydrioService.canDelete(seq)
    }
}