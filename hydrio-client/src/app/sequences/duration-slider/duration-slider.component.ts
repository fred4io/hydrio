import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HParams, HUtils } from 'hydrio-shared';

@Component({
  selector: 'app-duration-slider',
  templateUrl: './duration-slider.component.html',
  styleUrls: ['./duration-slider.component.scss']
})
export class DurationSliderComponent implements OnInit {

  public secondsMax = HParams.itemDurationSecondsMax
  public secondsMin = HParams.itemDurationSecondsMin
  public tickIntervalSeconds = HParams.itemDurationTickIntervalSeconds
  public stepSeconds = HParams.itemDurationStepSeconds

  @Input() placeholder = "durée"

  @Input() seconds = 60
  @Output() secondsChange: EventEmitter<number> = new EventEmitter()

  @Input() disabled = false

  public time: string

  constructor() { }

  ngOnInit() {
    this.time = HUtils.secondsToMMSS(this.seconds)
  }

  public secondsMoved(v: any) {
    this.time = HUtils.secondsToMMSS(v.value)
    this.secondsChange.emit(v.value)
  }

}
