import * as moment from 'moment'
import { Component } from '@angular/core';
import { HydrioService, ErrorService, ConnectedComponentBase } from '../shared';
import { HSequence, HStatus, HPItem, HUtils, HParams } from 'hydrio-shared';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends ConnectedComponentBase {

  public sequences: HSequence[]
  public activeSequence: HSequence
  public locked: boolean
  public status: HStatus
  public showPoll: boolean
  public serverTimeout: boolean
  private serverTimeoutCount = 0

  private pollTimer: any
  private showPollTimer: any
  private timeoutTimer: any

  constructor(
    hydrioService: HydrioService,
    private errorService: ErrorService
  ) {
    super(hydrioService)
  }

  formatHHmm(m: moment.Moment) {
    return !m ? '' : m.format(HParams.statusShowSeconds ? 'HH:mm:ss' : 'HH:mm')
  }

  get seqNbCycles() { return this.activeSequence && this.activeSequence.repeat }

  getInfo(o: HPItem, info: string) {
    if (!o) { return '' }
    switch (info) {
      case 'name': return o.item && o.item.name
      case 'at': return !o.start ? '' : (HUtils.isTomorrow(o.start) ? "demain à" : "à")
      case 'start': return !o.start ? '' : this.formatHHmm(o.start)
      case 'until': return !o.end ? '' : (HUtils.isTomorrow(o.end) ? "jusqu'à demain" : "jusqu'à")
      case 'end': return !o.end ? '' : this.formatHHmm(o.end)
    }
  }

  protected init() {
    return this.hydrioService.getSequences()
      .then(result => {
        this.sequences = result
      }).then(() => {
        this.activeSequence = this.hydrioService.getActiveSequence()
        this.locked = this.hydrioService.getLocked()
        this.loopUpdateStatus()
      })
  }

  protected destroy() {
    clearTimeout(this.pollTimer)
    clearTimeout(this.showPollTimer)
    clearTimeout(this.timeoutTimer)
  }

  private loopUpdateStatus() {
    if (this.destroying) { return }

    this.showPoll = true

    this.hydrioService.getStatus()
      .then(status => {
        this.serverTimeout = false
        this.serverTimeoutCount = 0
        if (!this.destroying) {
          this.status = status
          // console.log(status)
          this.showPollTimer = setTimeout(() =>
            this.showPoll = false,
            HParams.statusPollSymbolIntervalMs)
        }
      })
      .catch(err => {
        if (++this.serverTimeoutCount >= 3) {
          this.serverTimeout = true
          this.errorService.signal(err, 'loopUpdateStatus')
        }
      })
      .then(() => {
        if (!this.destroying) {
          this.pollTimer = setTimeout(
            () => this.loopUpdateStatus(),
            HParams.statusPollIntervalMs)
        }
      })
  }

  public activeSequenceChanged(seq: HSequence) {
    this.hydrioService.setActiveSequence(seq)
      .catch(err => this.errorService.signal(err, 'setActiveSequence'))
  }

  public setLocked(locked: boolean) {
    this.hydrioService.setLocked(locked).then(() => {
      this.locked = this.hydrioService.getLocked()
    })
      .catch(err => this.errorService.signal(err, 'setLocked'))
  }
}

