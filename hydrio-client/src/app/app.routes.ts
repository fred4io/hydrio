import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NoContentComponent } from './no-content/no-content.component';
import { DevicesComponent } from './devices/devices.component';
import { SequenceListComponent } from './sequences/sequence-list/sequence-list.component';
import { SequenceComponent } from './sequences/sequence/sequence.component';
import { ToolsComponent } from './tools/tools.component';

// import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'devices', component: DevicesComponent },
  { path: 'sequences', component: SequenceListComponent },
  { path: 'sequence/:id', component: SequenceComponent },
  { path: 'tools', component: ToolsComponent },
  { path: '**', component: NoContentComponent },
];
