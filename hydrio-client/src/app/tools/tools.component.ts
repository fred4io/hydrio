
import { Component, HostBinding } from '@angular/core'
import { ConnectedComponentBase, HydrioService, ConfirmDialogService } from '../shared'
import { HParams, HSystemInfo } from 'hydrio-shared'
import { OverlayContainer } from '@angular/cdk/overlay'

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent extends ConnectedComponentBase {
  @HostBinding('class') componentCssClass

  isReady = false
  showSimulatePowerLoss = HParams.powerLossDetection
  sysInfo: HSystemInfo

  constructor(
    hydrioService: HydrioService,
    private confirm: ConfirmDialogService,
    public overlayContainer: OverlayContainer
  ) {
    super(hydrioService)
  }

  protected init(done: () => void) {
    this.sysInfo = this.hydrioService.sysInfo
    done()
  }

  setSystemTimeClicked() {
    this.confirm.show("Ajuster l'heure")
      .then(confirmed => {
        if (confirmed) { this.hydrioService.setSystemTime() }
      })
  }

  shutdownClicked() {
    this.confirm.show("Eteindre le système")
      .then(confirmed => {
        if (confirmed) { this.hydrioService.shutdown() }
      })
  }

  simulatePowerLossClicked() {
    this.confirm.show("Simuler une panne de courant")
      .then(confirmed => {
        if (confirmed) { this.hydrioService.simulatePowerLoss() }
      })
  }

  onSetTheme(theme) {
    this.overlayContainer.getContainerElement().classList.add(theme);
    this.componentCssClass = theme;
  }
}
