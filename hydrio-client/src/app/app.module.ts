import { LOCALE_ID, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { RouterModule, Routes } from '@angular/router'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { HttpClientModule } from '@angular/common/http'
import { registerLocaleData } from '@angular/common'
import localeFr from '@angular/common/locales/fr'
import { OverlayModule} from '@angular/cdk/overlay'

registerLocaleData(localeFr, 'fr')

import {
  MatToolbarModule,
  MatListModule,
  MatGridListModule,
  MatButtonModule,
  MatDialogModule,
  MatInputModule,
  MatIconModule,
  MatFormFieldModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatButtonToggleModule,
  MatCheckboxModule,
  MatTableModule,
  MatSliderModule,
  MatOptionModule,
  MatExpansionModule
} from '@angular/material'
import 'hammerjs'

import { AppComponent } from './app.component'
import { ROUTES } from './app.routes'
import {
  HydrioService, ConfirmDialogComponent, ConfirmDialogService, ErrorService,
  SecondsToHMMSSPipe, SecondsToHMMPipe, SecondsToMMSSPipe,
  ValidSelectedDeviceDirective,
  YesNoComponent
} from './shared'
import { HomeComponent } from './home/home.component'
import { NoContentComponent } from './no-content/no-content.component'
import { DevicesComponent } from './devices/devices.component'
import { SequenceListComponent } from './sequences/sequence-list/sequence-list.component'
import { SequenceComponent } from './sequences/sequence/sequence.component'
import { ItemEditComponent } from './sequences/item-edit/item-edit.component'
import { SequenceGeneratorComponent } from './sequences/sequence-generator/sequence-generator.component'
import { DurationSliderComponent } from './sequences/duration-slider/duration-slider.component'
import { ToolsComponent } from './tools/tools.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NoContentComponent,
    DevicesComponent,
    SequenceListComponent,
    SequenceComponent,
    SecondsToHMMSSPipe,
    SecondsToHMMPipe,
    SecondsToMMSSPipe,
    ItemEditComponent,
    ConfirmDialogComponent,
    SequenceGeneratorComponent,
    DurationSliderComponent,
    ValidSelectedDeviceDirective,
    ToolsComponent,
    YesNoComponent
  ],
  entryComponents: [
    ItemEditComponent,
    ConfirmDialogComponent,
    SequenceGeneratorComponent
  ],
  imports: [
    RouterModule.forRoot(
      ROUTES,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatGridListModule,
    MatInputModule,
    MatFormFieldModule,
    MatListModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatSliderModule,
    MatTableModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    HttpClientModule,
    OverlayModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr' },
    ErrorService, HydrioService, ConfirmDialogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
