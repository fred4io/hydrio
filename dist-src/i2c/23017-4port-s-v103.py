#-------------------------------------------------------------------------------#
# Name:         23017-4port-s-v103.py						#
# Purpose:      Raspberry Pi, 4 Port, Version 1.03, Simple GUI			#
# GPIO-Library: RPi.GPIO 0.5.3a							#
#										#
# Author:       Pridopia, James Clarke						#
# Website:      www.Pridopia.co.uk						#
#										#
# Created:      12/09/2013							#
#-------------------------------------------------------------------------------#

import smbus, sys, getopt, time, os, signal

Bus = smbus.SMBus(1)

def Read(Addr, Reg):
	Ret = Bus.read_byte_data(Addr, Reg)
	Ret = str( '{:08b}'.format( Ret ) )
	return Ret

def Write(Addr, Reg, Data):
	Bus.write_byte_data(Addr, Reg, Data)
	return 1

def handler(signum, frame):
	Write(0x20, 0x12, 0x00)
	Write(0x20, 0x13, 0x00)

	Write(0x21, 0x12, 0x00)
	Write(0x21, 0x13, 0x00)

	Write(0x22, 0x12, 0x00)
	Write(0x22, 0x13, 0x00)

	Write(0x23, 0x12, 0x00)
	Write(0x23, 0x13, 0x00)
	print("Ctrl + Z Pressed. Quitting")
	sys.exit()

def Quit():
	Write(0x20, 0x12, 0x00)
	Write(0x20, 0x13, 0x00)

	Write(0x21, 0x12, 0x00)
	Write(0x21, 0x13, 0x00)

	Write(0x22, 0x12, 0x00)
	Write(0x22, 0x13, 0x00)

	Write(0x23, 0x12, 0x00)
	Write(0x23, 0x13, 0x00)
	print("Quitting.")
	sys.exit()


signal.signal(signal.SIGTSTP, handler)

def ChangeLed(Input):
	Bank = Input[0]
	Port = Input[1]
	Led  = Input[2]
	Bank = Bank.upper()
	if Bank == "A":
		Addr = 0x20
	elif Bank == "B":
		Addr = 0x21
	elif Bank == "C":
		Addr = 0x22
	elif Bank == "D":
		Addr = 0x23
	else:
		sys.exit()
	
	if Led == "1":
		Out = "00000001"
	elif Led == "2":
		Out = "00000010"
	elif Led == "3":
		Out = "00000100"
	elif Led == "4":
		Out = "00001000"
	elif Led == "5":
		Out = "00010000"
	elif Led == "6":
		Out = "00100000"
	elif Led == "7":
		Out = "01000000"
	elif Led == "8":
		Out = "10000000"
	else:
		main("Incorrect LED selected")

	if Port == "1":
		Bin = bin(int(Read(Addr, 0x12), 2) ^ int(Out, 2))
		Write( Addr, 0x12, int(Bin, 2) )
		main("")
	if Port == "2":
		Bin = bin(int(Read(Addr, 0x13), 2) ^ int(Out, 2))
		Write( Addr, 0x13, int(Bin, 2) )
		main("")


def main(Error):

	Write(0x20, 0x00, 0x00)
	Write(0x20, 0x01, 0x00)

	Write(0x21, 0x00, 0x00)
	Write(0x21, 0x01, 0x00)

	Write(0x22, 0x00, 0x00)
	Write(0x22, 0x01, 0x00)

	Write(0x23, 0x00, 0x00)
	Write(0x23, 0x01, 0x00)

	try:
	    while True:
		os.system("clear")
		print("Output test for MCP23017")

		B1 = Read(0x20, 0x12)
		B2 = Read(0x20, 0x13)

		B3 = Read(0x21, 0x12)
		B4 = Read(0x21, 0x13)

		B5 = Read(0x22, 0x12)
		B6 = Read(0x22, 0x13)

		B7 = Read(0x23, 0x12)
		B8 = Read(0x23, 0x13)
		print("    8   7   6   5   4   3   2   1")
		print("A1 [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]" % (B1[0], B1[1], B1[2], B1[3], B1[4], B1[5], B1[6], B1[7]) )			
		print("A2 [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]" % (B2[0], B2[1], B2[2], B2[3], B2[4], B2[5], B2[6], B2[7]) )
		print("B1 [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]" % (B3[0], B3[1], B3[2], B3[3], B3[4], B3[5], B3[6], B3[7]) )
		print("B2 [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]" % (B4[0], B4[1], B4[2], B4[3], B4[4], B4[5], B4[6], B4[7]) )
		print("C1 [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]" % (B5[0], B5[1], B5[2], B5[3], B5[4], B5[5], B5[6], B5[7]) )
		print("C2 [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]" % (B6[0], B6[1], B6[2], B6[3], B6[4], B6[5], B6[6], B6[7]) )
		print("D1 [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]" % (B7[0], B7[1], B7[2], B7[3], B7[4], B7[5], B7[6], B7[7]) )
		print("D2 [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]" % (B8[0], B8[1], B8[2], B8[3], B8[4], B8[5], B8[6], B8[7]) )
		print(Error)

		print("Enter the Bank ( A-D ), Port ( 1-2 ) and LED number ( 1-8 ).")
		print("Type RES to Reset.")
                print( "Example \"A21\" or \"a21\" will Toggle Bank A, Port 2, LED 1.")                  
		Input = raw_input(">")
		Input = Input.upper()
		if Input == "RES":
			Quit()
		if Input[0] != "A" and Input[0] != "B" and Input[0] != "C" and Input[0] != "D":
			main("Incorrect Bank entered: "+Input[0])
		if len(Input) < 3:
			main("Incorrect Input.")
		elif len(Input) > 3:
			main("Too many letters / numbers.")
		else:
			ChangeLed(Input)
	except KeyboardInterrupt:
		Write(0x20, 0x12, 0x00)
		Write(0x20, 0x13, 0x00)
	
		Write(0x21, 0x12, 0x00)
		Write(0x21, 0x13, 0x00)
	
		Write(0x22, 0x12, 0x00)
		Write(0x22, 0x13, 0x00)
	
		Write(0x23, 0x12, 0x00)
		Write(0x23, 0x13, 0x00)
		print("Ctrl + C Pressed. Quitting")
		sys.exit()

if __name__ == '__main__':
    main("")
