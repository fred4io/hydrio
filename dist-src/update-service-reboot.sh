#!/bin/bash

##uninstall service
sudo service hydrio stop
sudo /opt/nodejs/bin/forever-service delete hydrio
if [ "$1" != "newdb" ] ; then
    echo copying db file back to dist
    sudo \cp -rf /usr/local/bin/hydrio/data/hydrio.db.sqlite3 ./server/data/
    echo done
fi
sudo rm -rf /usr/local/bin/hydrio/

sudo ./install-service.sh

echo rebooting...
sudo reboot