#!/bin/bash

echo installing hydrio service...

set -e

mkdir /usr/local/bin/hydrio ||:

cp -r server/* /usr/local/bin/hydrio

set +e

curdir=$PWD
cd /usr/local/bin/hydrio

/opt/nodejs/bin/forever-service install \
--foreverPath /opt/nodejs/bin \
--minUptime 5000 \
--spinSleepTime 2000 \
--logrotateDateExt \
--logrotateMax 7200 \
--envVars "PORT=80 DEBUG=\"node WebServer ExpressApp DbSqlite3 HydrioServer HydrioWebAPI HydrioController HydrioDeviceController HydrioPiHardwareController HydrioDB HydrioSequencer\"" \
--foreverOptions " -c \"node -r tsconfig-paths/register\"" \
--script server.js \
hydrio

cd "$curdir"