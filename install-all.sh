#!/bin/bash

cd hydrio-shared
npm install
cd ..

cd hydrio-client
npm rebuild node-sass --force
npm install
cd ..

cd hydrio-server
npm install
cd ..
